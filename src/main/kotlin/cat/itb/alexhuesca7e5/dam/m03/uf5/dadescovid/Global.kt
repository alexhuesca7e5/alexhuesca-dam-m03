package cat.itb.alexhuesca7e5.dam.m03.uf5.dadescovid

import kotlinx.serialization.Serializable

@Serializable
data class Global(
    val Date: String,
    val NewConfirmed: Int,
    val NewDeaths: Int,
    val NewRecovered: Int,
    val TotalConfirmed: Int,
    val TotalDeaths: Int,
    val TotalRecovered: Int
)