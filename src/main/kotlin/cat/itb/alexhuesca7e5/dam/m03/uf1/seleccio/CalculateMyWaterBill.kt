package cat.itb.alexhuesca7e5.dam.m03.uf1.seleccio

import java.util.*

fun main(){
    val scn= Scanner(System.`in`)
    println("Lletra del tipus d'habitatge:")
    val lletra:String= scn.next()
    println("Número m^3 d'aigua gastats:")
    val numeroAigua:Double= scn.nextDouble()
    val quota= when(lletra){
        "A"-> 2.46
        "B"-> 6.40
        "C"->7.25
        "D"->11.21
        "E"->12.10
        "F"->17.28
        "G"->28.01
        "H"->40.50
        "I"->61.31
        else->0.0
    }
    val tarifes= when(numeroAigua){
        in 0.0..6.0-> 0.5849
        in 7.0..9.0->1.1699
        in 10.0..15.0->1.7548
        in 16.0..18.0->2.3397
        else-> 2.9246
    }
    println(quota+(tarifes*numeroAigua))


}