package cat.itb.alexhuesca7e5.dam.m03.uf1.lists

import java.util.*

fun main() {
    val casos: List<Int> = readIntList(Scanner(System.`in`))
    val infeccio= mutableListOf<Double>()
    for (i in 1..casos.lastIndex){
        val operation= casos[i].toDouble()/casos[i-1].toDouble()
        infeccio.add(operation)
    }
    println(infeccio)
}