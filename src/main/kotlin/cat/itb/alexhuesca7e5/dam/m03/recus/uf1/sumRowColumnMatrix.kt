package cat.itb.alexhuesca7e5.dam.m03.recus.uf1

import java.util.*

fun main() {
    val scn = Scanner(System.`in`)
    val matrix1 = MutableList<MutableList<Int>>(3){ MutableList(3){scn.nextInt()} }
    for (i in matrix1.indices){
        var sumCol= 0
        var sumRow= 0
        for (j in matrix1.indices){
            sumRow += matrix1[i][j]
            sumCol += matrix1[j][i]
        }
        println("Sum of row ${i+1}: $sumRow")
        println("Sum of column ${i+1}: $sumCol")
    }
}