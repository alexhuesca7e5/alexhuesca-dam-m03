package cat.itb.alexhuesca7e5.dam.m03.uf5.exercices.lists

import java.util.*

data class Country(var nom:String, var capital:String, val superficie:Int, val densitat:Int){

}

fun main() {
    val scanner = Scanner(System.`in`)
    val integerList = readCountry(scanner)
    val printCountries = integerList.sortedBy{ it.capital }.filter { country -> country.densitat > 5 && country.superficie > 1000  }
    printCountries.forEach { println(it.nom) }
}

val readCountry :(Scanner) -> List<Country> = { scanner ->
    val size = scanner.nextInt()
    val list = mutableListOf<Country>()
    repeat(size){list.add(Country(scanner.next(),scanner.next(),scanner.nextInt(),scanner.nextInt()))}
    list
}
