package cat.itb.alexhuesca7e5.dam.m03.uf3.exercices.generalexam

import java.io.File
import java.util.*
import kotlin.io.path.*

/**
 * He creat un arxiu anomenat file.txt al directori resources ("src/main/resources/file.txt") i l'he copiat a "src/main/resources/cat/itb/alexhuesca7e5/dam/m03/uf3/exercices"
 * per comprobar si el programa funciona correctament
  */
fun main() {
    val scn = Scanner(System.`in`)
    val filePath = scn.nextLine()
    val destinationPath = scn.nextLine()
    val file = File(filePath)
    val name = scn.nextLine()
    val surname = scn.nextLine()
    if (file.exists()) {
        if (file.isDirectory) {
            println("No es pot copiar una carpeta")
        } else {
            val copiedFileDest = file.copyTo(Path(destinationPath.toString(), name, surname, file.name).toFile())
            println("Ha copiat el ${file.name} a la carpeta $copiedFileDest")
        }
    }
}