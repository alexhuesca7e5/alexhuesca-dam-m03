package cat.itb.alexhuesca7e5.dam.m03.uf1.seleccio

import java.time.Year
import java.util.*

fun main(){
    println("Introdueix una hora amb tres enters")
    val scn= Scanner(System.`in`)
    var hora:Int= scn.nextInt()
    var minuts:Int= scn.nextInt()
    var segons:Int= scn.nextInt()
    if(segons==59){
        segons= 0
        if(minuts== 59){
            minuts= 0
            if(hora== 23){
                hora= 0
            }else{
                hora++
            }
        }else{
            minuts++
        }

    }else{
        segons++
    }
    println("$hora:$minuts:$segons")
}
