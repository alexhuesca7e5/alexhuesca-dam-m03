package cat.itb.alexhuesca7e5.dam.m03.uf2.functions

import java.util.*

data class Lamp(var activate:Boolean){
    fun activateLamp(action:String):Boolean{
        when(action){
            "TURN OFF" -> this.activate=false
            "TURN ON" -> this.activate=true
            "TOGGLE" -> this.activate = !(this.activate)
        }
        return this.activate
    }

}

fun main() {
    val scn= Scanner(System.`in`)
    var inputValue: String
    var actionLamp= Lamp(activate = false)
    while (true){
        inputValue=scn.nextLine()
        if (inputValue=="END")break
        println(actionLamp.activateLamp(action = inputValue))
    }
}