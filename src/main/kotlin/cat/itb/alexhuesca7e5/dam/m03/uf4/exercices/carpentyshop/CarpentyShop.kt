package cat.itb.alexhuesca7e5.dam.m03.uf4.exercices.carpentyshop

import java.util.*

abstract class Product {
    abstract val calculateTotalPrice:Int

    class Taulells(val price: Int, val lenght: Int, val with: Int): Product() {
        override val calculateTotalPrice: Int
            get() = (this.lenght * this.with) * this.price
    }


    class Llisto(val price: Int, val length: Int):Product(){
        override val calculateTotalPrice: Int
            get() = this.price * this.length
    }

}

fun main() {
    val scn = Scanner(System.`in`)
    val list = mutableListOf<Product>()
    var material:String =""
    var preu:Int
    var llargada:Int
    var amplada:Int
    while (true) {
        material = scn.next()
        if (material == "END") break
        if (material == "Taulell") {
            preu = scn.nextInt()
            llargada = scn.nextInt()
            amplada = scn.nextInt()
            val taulell = Product.Taulells(preu, llargada, amplada)
            list.add(taulell)
        } else if (material == "Llistó") {
            preu = scn.nextInt()
            llargada = scn.nextInt()
            val llisto = Product.Llisto(preu, llargada)
           list.add(llisto)
        }
    }
    val totalPrice = list.sumOf { it.calculateTotalPrice }
    println("El preu total és: $totalPrice €")
}