package cat.itb.alexhuesca7e5.dam.m03.uf1.data

import java.util.*
fun velocitatMitjana (kilometre: Double, minuts: Int)= kilometre/(minuts/60)
fun main(){
    println("Introdueix els kilometres recorreguts")
    val scn= Scanner(System.`in`)
    val km= scn.nextDouble()
    println("Introdueix el temps en minuts del recorregut")
    val min= scn.nextInt()
    val resultat= velocitatMitjana(km,min)
    println("La velocitat mitjana és de $resultat km/h")
}