package cat.itb.alexhuesca7e5.dam.m03.recus

import java.util.*

class UI(val scn: Scanner = Scanner(System.`in`).useLocale(Locale.UK), val animeApp: AnimeAPP){
    fun start() {
        showMenu()
        var option = scn.nextInt()
        while (option !=0){
            when(option){
                1 -> showAnimes()
                2 -> bestPunctuation()
                3 -> addAnime()
                4 -> deleteAnime()
                5 -> break
                else -> println("ERROR")
            }
            showMenu()
            option = scn.nextInt()
        }
    }

    private fun showAnimes() {
        animeApp.showAnimes()
    }

    private fun bestPunctuation() {
        println(animeApp.bestPunctuation())
    }

    private fun addAnime() {
        scn.nextLine()
        println("Enter a name:")
        val name = scn.nextLine()
        println("Enter the number os seasons:")
        val numSeasons = scn.nextInt()
        println("Enter a punctuation:")
        val punctuation = scn.nextDouble()
        if (animeApp.addAnime(Anime(name, numSeasons, punctuation))){
            println("Anime added correctly")
        }else {
            println("ERROR: Anime already exist!")
        }
    }

    private fun deleteAnime() {
        scn.nextLine()
        println("Select the title of the anime you want to delete:")
        val title = scn.nextLine()
        if (!animeApp.checkAnime(title)){
            println("Anime doesn't exist!")
        } else {
            animeApp.deleteAnime(title)
            println("Anime successfully removed")
        }
    }

    fun showMenu(){
        println("Select an option:\n"+"1.Show Animes\n"+"2.Show best rated Anime\n"+"3.Add anime\n"+"4.Delete Anime\n"+"5.Exit")
    }
}

class AnimeAPP (val anime: MutableList<Anime>) {
    fun showAnimes() {
        anime.forEach { println(it)}
    }

    fun bestPunctuation():String? {
        return anime.maxByOrNull { it.punctuation }?.title
    }

    fun addAnime(anime: Anime):Boolean {
        return if (!checkAnime(anime.title)){
            this.anime.add(anime)
            true
        } else false
    }

    fun deleteAnime(title: String) {
        anime.remove(anime.find {it.title == title})
    }
    fun checkAnime(title: String):Boolean{
        return anime.any { it.title == title }
    }
}
