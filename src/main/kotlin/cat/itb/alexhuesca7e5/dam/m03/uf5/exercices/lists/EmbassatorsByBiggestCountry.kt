package cat.itb.alexhuesca7e5.dam.m03.uf5.exercices.lists

import java.util.*

data class Embassators (val name:String, val surname:String, val country: Country?){
    override fun toString(): String {
        return "$name $surname"
    }
}

fun main() {
    val scanner = Scanner(System.`in`)
    val countryList = readCountry(scanner)
    val embassatorsList = readEmbassators(scanner,countryList).sortedBy { it.country?.superficie }
    embassatorsList.forEach { println(it) }
}

val readEmbassators :(Scanner,List<Country>) -> List<Embassators> = { scanner,listc ->
    val size = scanner.nextInt()
    val list = mutableListOf<Embassators>()
    repeat(size){
        val name = scanner.next()
        val surname = scanner.next()
        val country = scanner.next()
        val c = listc.find { it.nom == country }
        list.add(Embassators(name,surname,c))
    }
    list
}