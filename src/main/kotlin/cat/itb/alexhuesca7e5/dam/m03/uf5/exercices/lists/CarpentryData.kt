package cat.itb.alexhuesca7e5.dam.m03.uf5.exercices.lists

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val count = scanner.nextInt()
    val products = List(count){
        readProduct(scanner)
    }
    val priceLlistons = products.filter { it is Llisto }.sumOf { it.totalPrice }
    val maxLength = products.maxByOrNull { it.totalPrice }?.lenght
    val higherThan100 = products.count{it.totalPrice > 100}
    println("Preu total llistons: $priceLlistons")
    println("Producte més car té una llargada de: $maxLength")
    println("Num productés de més de 100€: $higherThan100")
}


fun readProduct(scanner: Scanner) : Product{
    val type = scanner.next()
    return when(type){
        "Taulell" -> readTaulell(scanner)
        else -> readLlisto(scanner)
    }
}

fun readLlisto(scanner: Scanner) =
    Llisto(scanner.nextInt(), scanner.nextInt())

fun readTaulell(scanner: Scanner) =
    Taulell(scanner.nextInt(), scanner.nextInt(), scanner.nextInt())


abstract class Product(val unitPrice : Int, val lenght :Int) {
    abstract val size: Int
    val totalPrice get() = size * unitPrice
}

class Taulell(unitPrice: Int, lenght: Int, val width: Int)
    :Product(unitPrice, lenght){
    override val size = lenght*width
}

class Llisto(unitPrice: Int, lenght: Int)
    :Product(unitPrice, lenght){

    override val size = lenght
}
