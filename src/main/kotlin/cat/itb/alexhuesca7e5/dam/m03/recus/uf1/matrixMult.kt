package cat.itb.alexhuesca7e5.dam.m03.recus.uf1

import java.util.*

fun main() {
    val scn = Scanner(System.`in`)
    val matrix1 = MutableList<MutableList<Int>>(3){ MutableList(3){scn.nextInt()} }
    val result = MutableList<MutableList<Int>>(3){ MutableList(3){0}}
    val multiplyer= scn.nextInt()
    for (i in matrix1.indices){
        for(j in matrix1.indices){
            result[i][j] = multiplyer * matrix1[i][j]
        }
    }
    result.forEach(::println)
}