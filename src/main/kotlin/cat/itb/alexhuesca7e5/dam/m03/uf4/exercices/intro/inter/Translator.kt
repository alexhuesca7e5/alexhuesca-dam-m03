package cat.itb.alexhuesca7e5.dam.m03.uf4.exercices.intro.inter

import java.util.*

interface TranslatorApp{
    fun translateText(textList:MutableList<Translator>)
}

class Translator(private val text:String): TranslatorApp {
    override fun translateText(textList: MutableList<Translator>) {
        textList.forEach { line ->
            println(line.text)
        }
    }
}

fun main() {
    val scn = Scanner(System.`in`)
    val list = mutableListOf<Translator>()
    var input:String= ""
    while (true){
        input = scn.nextLine()
        if (input.last().toChar() == '.') {
            val text = Translator(input)
            list.add(text)
            text.translateText(list)
            break
        }
        val text = Translator(input)
        list.add(text)
    }
}
