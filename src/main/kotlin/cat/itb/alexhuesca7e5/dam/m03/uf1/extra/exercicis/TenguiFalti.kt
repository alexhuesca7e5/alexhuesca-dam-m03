package cat.itb.alexhuesca7e5.dam.m03.uf1.extra.exercicis

import java.util.Scanner

fun main() {
    val scn = Scanner(System.`in`)
    val tingui = mutableListOf<Int>()
    val falti = mutableListOf<Int>()
    val repeated = mutableListOf<Int>()
    var inputValue = scn.nextInt()
    while (true){
        inputValue = scn.nextInt()
        if (inputValue == -1)break
        tingui.add(inputValue)
    }
    while (true){
        inputValue = scn.nextInt()
        if (inputValue == -1)break
        falti.add(inputValue)
    }
    for (i in tingui.indices){
        for (j in falti.indices){
            if (tingui[i]==falti[j]){
                repeated.add(tingui[i])
            }
        }
    }
    println(repeated.sorted())
}