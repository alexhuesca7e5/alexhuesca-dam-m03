package cat.itb.alexhuesca7e5.dam.m03.uf1.lists

fun main() {
    val map =listOf(
        listOf(1.5,1.6,1.8,1.7,1.6),
        listOf(1.5,2.6,2.8,2.7,1.6),
        listOf(1.5,4.6,4.4,4.9,1.6),
        listOf(2.5,1.6,3.8,7.7,3.6),
        listOf(1.5,2.6,3.8,2.7,1.6)
    )
    var maxValue=0.0
    var positionRow=0
    var positionColumn=0
    for (i in map.indices){
        for (j in map.indices){
            if(map[i][j]>maxValue){
                maxValue=map[i][j]
                positionRow=i
                positionColumn=j

            }

        }
    }
    println("$positionRow,$positionColumn: $maxValue")



}