package cat.itb.alexhuesca7e5.dam.m03.uf2.recursivity

import java.util.*

fun getFibonacci (numberOne:Int):Int{
     if (numberOne <= 1) return numberOne
    else{
        return  getFibonacci(numberOne-1) + getFibonacci(numberOne-2)
     }
}

fun main() {
    val scn = Scanner(System.`in`)
    val number = scn.nextInt()
    println(getFibonacci(number))
}

