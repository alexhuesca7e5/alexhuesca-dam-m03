package cat.itb.alexhuesca7e5.dam.m03.uf1.data.project

import java.time.Year
import java.util.*
import kotlin.math.pow

val scannerTA = Scanner(System.`in`).useLocale(Locale.UK)
fun main() {
    welcome("TrainingAssistant") // change it as you need
    ageCalculator()
    imcCalculator()
    trainingSchedule()
}

fun welcome(assistantName: String) {
    println("Hello! My name is TrainingAssistant.")
    println("Please, tell me your name.")
    val name= Scanner(System.`in`).next()
    println("What a great name you have, $name!")
}
fun ageCalculator() {
    println("Please, tell me which year you were born.")
    val year= Scanner(System.`in`).nextInt()
    val age= Year.now().value-year
    val age2= age+1
    println ("You are between $age - $age2 years old. That's a good age for practicing sport.")
}

fun imcCalculator() {
    println("Let's check some of your parameters\n" +
            "Tell me your weight in kg")
    val scn=Scanner(System.`in`)
    val weight= scn.nextDouble()
    println("Tell me your height in m")
    val height= scn.nextDouble()
    val imc= weight/ height.pow(2.0)
    println("Your IMC is $imc")
    val insW= imc<18.5
    val normW= imc in 18.5..24.9
    val overweight= imc >25 && imc<50.0
    val obesity= imc>50.0
    println("Checking insufficient weight...$insW")
    println("Checking normal weight...$normW")
    println("Checking overweight...$overweight")
    println("Checking obesity...$obesity")
}
fun trainingSchedule() {
    println("I'll tell you your training plan.\n"+"How many hours would you like to train?")
    val scn= Scanner(System.`in`)
    val hours:Int = scn.nextInt()
    println("How many days can you train?")
    val days= scn.nextInt()
    val daysRoutine1= hours%days
    val hoursRoutine1= (hours/days)+1
    val daysRoutine2= days-(hours%days)
    val hoursRoutine2= hours/days
    println("Your routine sport could be:\n"+
    "$daysRoutine1 days $hoursRoutine1 hours\n"+
    "$daysRoutine2 days $hoursRoutine2 hours")
}
