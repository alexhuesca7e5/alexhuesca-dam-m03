package cat.itb.alexhuesca7e5.dam.m03.uf1.lists

import java.util.*

fun scannerReadIntMatrix(scanner: Scanner ):List<List<Int>>{
    var row=scanner.nextInt()
    var colum=scanner.nextInt()
    val list= List(row){List(colum){scanner.nextInt()}}
    return list
}