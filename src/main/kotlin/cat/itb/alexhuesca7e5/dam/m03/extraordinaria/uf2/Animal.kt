package cat.itb.alexhuesca7e5.dam.m03.extraordinaria.uf2

data class Animal(val codi:String, val nom:String, val tipus:String ,val numApats:Int, val quantMenjar:Int){
    val total get() = numApats * quantMenjar
}