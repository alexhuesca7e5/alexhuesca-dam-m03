package cat.itb.alexhuesca7e5.dam.m03.uf2.recursivity

import cat.itb.alexhuesca7e5.dam.m03.uf1.lists.readIntList
import java.util.Scanner


fun listMaxValue(list: MutableList<Int>): List<Int> {
    if (list.size == 1){
        return list
    }
    else {
        if (list[0]>list[1]){
            list.removeAt(1)
        } else list.removeAt(0)
    }
    return listMaxValue(list)
}

fun main() {
    val scn = Scanner(System.`in`)
    val list = readIntList(scn)
    println(listMaxValue(list.toMutableList()).first())
}