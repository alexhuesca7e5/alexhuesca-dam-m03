package cat.itb.alexhuesca7e5.dam.m03.uf1.mix

import java.util.*

fun main() {
    val scn=Scanner(System.`in`)
    val matchPoints= mutableListOf<Int>(0)
    var onePoint=0
    var twoPoint=0
    var threePoint=0
    while (true){
        val points=scn.nextInt()
        if(points==-1)break
        matchPoints.add(points)
    }
    for (i in 1..matchPoints.lastIndex){
        if (matchPoints[i]-matchPoints[i-1]==3)threePoint++
        if (matchPoints[i]-matchPoints[i-1]==2)twoPoint++
        if (matchPoints[i]-matchPoints[i-1]==1)onePoint++
    }
    println("cistelles d'un punt: $onePoint\n" +
            "cistelles de dos punts: $twoPoint\n" +
            "cistelles de tres punts: $threePoint")
}