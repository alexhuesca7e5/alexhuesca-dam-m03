package cat.itb.alexhuesca7e5.dam.m03.recus.alumnos

import cat.itb.alexhuesca7e5.dam.m03.uf5.exam.scn
import java.util.*

class UI (val scn:Scanner = Scanner(System.`in`).useLocale(Locale.UK), val student:StudentApp){
    fun showMenu(){
        println("1. Add a student\n" +
                "2. Student list\n" +
                "3. Students who have obtained a 10\n" +
                "4. Students who have suspended\n" +
                "5. Students who have obtained a 0\n" +
                "6. Students who have passed\n" +
                "7. Delete student\n" +
                "0. Exit")
    }
    fun start(){
        showMenu()
        var option = scn.nextInt()
        while (option !=0){
            when(option){
                1 -> addStudent()
                2 -> listStudent()
                3 -> show10Students()
                4 -> showSuspendedStudents()
                5 -> show0Students()
                6 -> showPassedStudents()
                7 -> deleteStudent()
                else -> println("ERROR")
            }
            showMenu()
            option = scn.nextInt()
        }
    }

    private fun addStudent() {
        scn.nextLine()
        println("Enter a name:")
        val name = scn.nextLine()
        println("Enter a mark:")
        val mark = scn.nextDouble()
        student.addStudent(Student(name,mark))
        println("Student added!")
    }

    private fun listStudent() {
        student.listStudent()
    }

    private fun show10Students() {
        student.show10Students().forEach {student -> println(student.name)}
    }

    private fun showSuspendedStudents() {
        student.showSuspendedStudents().forEach { println(it) }
    }

    private fun show0Students() {
        student.show0Students().forEach {student -> println(student.name)}
    }

    private fun showPassedStudents() {
        student.showPassedStudents().forEach { println(it) }
    }

    private fun deleteStudent() {
        scn.nextLine()
        println("Enter the name of the student you want to delete:")
        val name = scn.nextLine()
        student.deleteStudent(name)
        println("Student deleted!")
    }
}

data class StudentApp(val studentList:MutableList<Student>){
    fun addStudent(student:Student) {
        studentList.add(student)
    }
    fun listStudent(){
        studentList.forEach { println(it) }
    }
    fun show10Students():List<Student>{
       return studentList.filter { it.mark == 10.0 }
    }
    fun showSuspendedStudents():List<Student>{
        return studentList.filter { it.mark < 5.0 }
    }
    fun show0Students():List<Student>{
        return studentList.filter { it.mark == 0.0 }
    }
    fun showPassedStudents():List<Student>{
       return studentList.filter { it.mark >= 5.0 }
    }
    fun deleteStudent(name:String){
        studentList.remove(studentList.find { it.name == name })
    }
}