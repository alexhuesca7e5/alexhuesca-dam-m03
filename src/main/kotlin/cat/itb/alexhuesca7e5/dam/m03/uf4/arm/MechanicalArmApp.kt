package cat.itb.alexhuesca7e5.dam.m03.uf4.arm

data class MechanicalArmApp(var openAngle:Double = 0.0, var altitude:Double = 0.0, var turnedOn:Boolean = false ){
    fun toggle(){
        if (!this.turnedOn){
            turnedOn = true
        }
        else if (this.turnedOn) {
            turnedOn = false
        }
        println(this)
    }
    fun updateAltitude(num:Int){
        altitude += num.toDouble()
        println(this)
    }
    fun updateAngle(num:Int){
        openAngle+=num.toDouble()
        println(this)
    }
}

fun main() {
    val arm = MechanicalArmApp()
    arm.toggle()
    arm.updateAltitude(3)
    arm.updateAngle(180)
    arm.updateAltitude(-3)
    arm.updateAngle(-180)
    arm.updateAltitude(3)
    arm.toggle()
}