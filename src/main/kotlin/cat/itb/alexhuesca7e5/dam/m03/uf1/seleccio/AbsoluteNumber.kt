package cat.itb.alexhuesca7e5.dam.m03.uf1.seleccio

import java.util.*
import kotlin.math.abs

fun absoluteNumber(x:Int){
    if (x<0){
        println(abs(x))
    }else{
        println(x)
    }
}
fun main(){
    val num= Scanner(System.`in`).nextInt()
    absoluteNumber(num)
}
