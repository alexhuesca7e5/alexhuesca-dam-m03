    package cat.itb.alexhuesca7e5.dam.m03.uf1.lists

import java.util.*

fun main() {
    val scn = Scanner(System.`in`)
    val boxes = List(4){ MutableList(4){0} }
    var fila = 0
    var columna : Int= 0
    while (true){
        fila = scn.nextInt()
        if (fila == -1) break
        columna = scn.nextInt()
        boxes[fila][columna]++
    }
    println(boxes)
}