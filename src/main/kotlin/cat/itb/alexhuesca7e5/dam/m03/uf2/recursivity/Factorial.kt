package cat.itb.alexhuesca7e5.dam.m03.uf2.recursivity

import java.util.*

fun factorial (num:Int):Int{
    return if (num==0) 1
    else num* factorial(num-1)
}

fun main() {
    val scn= Scanner(System.`in`)
    val number = scn.nextInt()
    println(factorial(number))
}