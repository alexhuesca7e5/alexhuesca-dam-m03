package cat.itb.alexhuesca7e5.dam.m03.uf2.generalexam

class Functions (){

    val oilList = mutableListOf<Oil>()
    fun addOilSpill(name: String, company: String, liters: String, toxicity: String){
       val oil = Oil(name,company,liters,toxicity)
       oilList.add(oil)
    }

    fun getGravity(liters: String, toxicity: String): Double {
        return liters.toDouble() * toxicity.toDouble()
    }
}