package cat.itb.alexhuesca7e5.dam.m03.uf4.exercices.intro

abstract class Model {
    abstract val name:String
    abstract val brand: VehicleBrand
    abstract override fun toString():String
}
    data class VehicleBrand(val name: String, val country: String) {
        override fun toString(): String {
            return "name:$name, country:$country"
        }
    }

   data class BicycleMod(override val name: String, val gears: Int, override val brand: VehicleBrand) : Model() {
        override fun toString(): String {
            return "name:$name, gears:$gears, brand:$brand"
        }
    }

    class ScootereModel(override val name: String, val power: Double, override val brand: VehicleBrand) : Model() {
        override fun toString(): String {
            return "name:$name, power:$power,brand:$brand"
        }
    }

fun main() {
    val bici = BicycleMod("model X",5, VehicleBrand("Lorem","Togo"))
    val scooter = ScootereModel("Hotwalk",45.3, VehicleBrand("lorem","Togo"))
    println(bici)
    println(scooter)
}