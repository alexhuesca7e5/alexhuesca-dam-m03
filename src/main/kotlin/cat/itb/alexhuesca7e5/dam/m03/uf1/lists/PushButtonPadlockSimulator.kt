package cat.itb.alexhuesca7e5.dam.m03.uf1.lists

import java.util.*

fun main() {
    val scn= Scanner(System.`in`)
    val candado= MutableList(8){false}
    var num= scn.nextInt()
    while(num != -1){
        candado[num] = !candado[num]
        num=scn.nextInt()
    }
    println(candado)
}