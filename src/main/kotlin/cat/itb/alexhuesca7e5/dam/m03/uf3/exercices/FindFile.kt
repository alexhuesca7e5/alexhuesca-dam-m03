package cat.itb.alexhuesca7e5.dam.m03.uf3.exercices

import java.util.*
import kotlin.io.path.*


fun main() {
    val scn = Scanner(System.`in`)
    val userPath = scn.nextLine()
    val search = Path(userPath)
    val userFile = Path("$search/${scn.next()}")
    search.toFile().walk().forEach { file ->
        if (file.isFile && file.name == userFile.name) {
            println("${file.parent}/${file.name}")
        }
    }
}