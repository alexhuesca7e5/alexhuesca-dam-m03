package cat.itb.alexhuesca7e5.dam.m03.uf1.seleccio.exam

import java.util.*

fun main(){
    val scn=Scanner(System.`in`)
    val speed=scn.nextInt()
    if(speed in 0..120){
        println("Correcte")
    }
    if(speed in 121..140){
        println("Multa lleu")
    }
    if (speed>140){
        println("Multa greu")
    }
}