package cat.itb.alexhuesca7e5.dam.m03.uf1.lists

import java.util.*

fun main() {
    val listOne = readIntList(Scanner(System.`in`))
    val listTwo = readIntList(Scanner(System.`in`))
    for (i in 0..listOne.lastIndex) {
        if (listOne[i] == listTwo[i] && listOne == listTwo) {
            println("Són iguals")
            break
        } else {
            println("no són iguals")
            break
        }
    }
}

////També es podria fer només amb un if:
//    var i = 0
//    if (listOne[i] == listTwo[i] && listOne == listTwo) {
//        println("Són iguals")
//        i += 1
//    } else {
//        println("No són iguals")
//    }
//}
