package cat.itb.alexhuesca7e5.dam.m03.uf4.exercices.intro.instrumentsimulator

fun main(args: Array<String>) {
    val instruments: List<Instrument> = listOf(
        Triangle(5),
        Drump("A"),
        Drump("O"),
        Triangle(1),
        Triangle(5)
    )
    play(instruments)
}

private fun play(instruments: List<Instrument>) {
    for (instrument in instruments) {
        instrument.makeSounds(4) // plays 2 times the sound
    }
}