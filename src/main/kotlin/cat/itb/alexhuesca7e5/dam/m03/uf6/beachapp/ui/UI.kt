package cat.itb.alexhuesca7e5.dam.m03.uf6.beachapp.ui

import cat.itb.alexhuesca7e5.dam.m03.uf6.beachapp.data.BeachesDao
import cat.itb.alexhuesca7e5.dam.m03.uf6.beachapp.data.BeachesDatabase
import cat.itb.alexhuesca7e5.dam.m03.uf6.beachapp.models.Beach
import java.util.*

class BeachApp(val scanner: Scanner = Scanner(System.`in`), val database: BeachesDatabase = BeachesDatabase()) {
    val beachDao = BeachesDao(database)

    fun start(){
        database.connect().use { connection ->
            beachDao.createTableIfNotExists()
            executeOperations()
        }
    }

    fun showMenu(){
        println("Vols (0) sortir, (1) afegir, (2) modificar qualitat, (3) llistar, (4) resum?")
    }

    fun executeOperations() {
        showMenu()
        var option = scanner.nextInt()
        while(option != 0){
            when(option){
                1 -> addBeach()
                2 -> modifyWaterQuality()
                3 -> listContent()
                4 -> showSummary()
                else -> println("Invalid option")
            }
            if (true) {
                showMenu()
                option = scanner.nextInt()
            }
        }
    }

    private fun addBeach() {
        println("Introdueix l'identificador, nom, ciutat i qualitat")
        val id = scanner.nextInt()
        if(checkId(id)){
            println("Ja existeix l'ID!")
        } else{
            val name = scanner.next()
            val city = scanner.next()
            val quality = scanner.nextInt()
            if (quality > 5 || quality < 1){
                println("La qualitat ha d'anar de 1 a 5!")
            }
            else beachDao.insert(Beach(id,name,city,quality))
        }
    }
    /*private fun readBeach(scanner: Scanner): Beach {
        return Beach(scanner.nextInt(), scanner.next(),scanner.next(),scanner.nextInt())
    }
     */
    private fun modifyWaterQuality() {
        val id = scanner.nextInt()
        if (checkId(id)){
            val beach = beachDao.list().find { it.id == id }
            beach!!.quality = scanner.nextInt()
            if (beach.quality > 5 || beach.quality < 1){
                println("La qualitat ha d'anar de 1 a 5!")
            } else {
                beachDao.updateQuality(beach)
            }
        } else{
            println("Id no existent!")
        }
    }

    private fun listContent() {
        beachDao.list().forEach { println(it)}
    }

    private fun showSummary() {
        val cities = beachDao.list().map { it.city }.toSortedSet().sortedDescending()
        println(
        cities.joinToString ("\n"){ city ->
            val sum = beachDao.list().filter { it.city == city }.sumOf { it.quality }
            val avg = sum.toDouble()/beachDao.list().filter { it.city == city }.size.toDouble()
            "${cities.indexOf(city)}.$city ($avg)"
        }
        )
    }

    private fun checkId(id:Int):Boolean{
        return beachDao.list().find { beach -> beach.id == id } != null
    }
}