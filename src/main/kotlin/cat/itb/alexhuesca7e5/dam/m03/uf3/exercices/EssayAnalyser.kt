package cat.itb.alexhuesca7e5.dam.m03.uf3.exercices

import java.util.*
import kotlin.io.path.Path
import kotlin.io.path.readLines
import kotlin.io.path.readText

///dades/dades/Alex_Huesca

fun main() {
    val scn = Scanner(System.`in`)
    val userPath = scn.nextLine()
    val search = Path(userPath)
    val file = scn.nextLine()
    val userFile = Path("$search/$file")
    val readLines= userFile.readLines()
    val countLines = readLines.count()
    var wordCount = 0
    readLines.forEach(){
        wordCount += it.filter {
            it == ' '
        }.count()+1
    }
    print ("Número de línies: $countLines \n"+
    "Número de paraules: $wordCount")
}