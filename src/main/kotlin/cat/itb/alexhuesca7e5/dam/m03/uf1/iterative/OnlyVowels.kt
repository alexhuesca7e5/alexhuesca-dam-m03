package cat.itb.alexhuesca7e5.dam.m03.uf1.iterative

import java.util.*

fun main(){
    val scn= Scanner(System.`in`)
    val quantity= scn.nextInt()
    for(i in 1..quantity){
        var text= scn.next().single()
        when(text){
            'a','e','i','o','u'-> println(text)
        }
    }
    //Es pot fer d'una altra manera:
    //val scn=Scanner(System.in)
    //val times=scn.nextInt()
    //var letter:Char
    //repeat(times){
    //letter=scn.next().single()
    //when (letter){
    //'a','e','i','o','u'-> print(letter)



}