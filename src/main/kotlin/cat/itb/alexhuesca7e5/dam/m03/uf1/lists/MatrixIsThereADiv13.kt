package cat.itb.alexhuesca7e5.dam.m03.uf1.lists

fun main() {
    val matrix = listOf(
    listOf(2,5,1,6),
    listOf(23,52,14,36),
    listOf(23,75,81,62))
    println(matrix.any{ list -> list.any{num -> num % 13 == 0}})

}