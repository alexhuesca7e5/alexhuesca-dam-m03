package cat.itb.alexhuesca7e5.dam.m03.uf1.data

import java.util.*

//IsValidNote
fun main() {
    println("Introdueix el teu bitllet")
    val bitllet = Scanner(System.`in`).nextInt()
    println(bitllet == 5 || bitllet == 10 || bitllet == 20 || bitllet == 50 || bitllet == 100 || bitllet == 200)
//InRange
println("Escriu 5 nombres enters")
    val scn= Scanner(System.`in`)
    val num1 = scn.nextInt()
    val num2 = scn.nextInt()
    val num3 = scn.nextInt()
    val num4 = scn.nextInt()
    val num5 = scn.nextInt()
    val rang1= num1..num2
    val rang2= num3..num4
    println(num5 in rang1 && num5 in rang2)
//WorkingAge
    println("Esriu la teva edat")
    val years = Scanner(System.`in`).nextInt()
    val edatTreball= years in 16..65
    println("Pots treballar?")
    println(edatTreball)

}
