package cat.itb.alexhuesca7e5.dam.m03.uf5.dadescovid

import cat.itb.alexhuesca7e5.dam.m03.uf5.exercices.lists.Country
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.features.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.request.*
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import java.net.URL



suspend fun main() {

    val client = HttpClient(CIO){
        install(JsonFeature){
            serializer = KotlinxSerializer(kotlinx.serialization.json.Json {
                ignoreUnknownKeys = true
            })
        }
    }
    val covid : CovidData = client.get("https://api.covid19api.com/summary")
    for (i in 0..covid.Countries.lastIndex) {
        println(covid.Countries[i])
    }

    covid.printtop10Deaths()
    covid.printTop10Confirmed()
    covid.printTopEU()
    covid.printSpainDeaths()







}