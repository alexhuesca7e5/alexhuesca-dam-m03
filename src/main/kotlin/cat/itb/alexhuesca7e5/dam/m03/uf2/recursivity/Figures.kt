package cat.itb.alexhuesca7e5.dam.m03.uf2.recursivity

import java.util.*

fun figures(number:Int):Int {
     return if (number==0){
          0
     }
     else return 1+ figures(number/10)
}

fun main() {
     val scn = Scanner(System.`in`)
     val num = scn.nextInt()
     println(figures(num))
}


