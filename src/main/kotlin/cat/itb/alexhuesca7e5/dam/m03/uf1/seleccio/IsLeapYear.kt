package cat.itb.alexhuesca7e5.dam.m03.uf1.seleccio

import java.util.*

fun main(){
    val sc= Scanner(System.`in`)
    println("Introdueix un any:")
    val any= sc.nextInt()
        val darrersDigits= any%100
    val primersDigits=any/100
    if (darrersDigits%4==0) {
        if (darrersDigits == 0) {
            if(primersDigits % 4 == 0) { println("$any és any de traspàs")}
            else {println("$any no és any de traspàs")}
        }
        else{
            println("$any és any de traspàs")
        }
    }
    else{
        println("$any no és any de traspàs")
    }
}