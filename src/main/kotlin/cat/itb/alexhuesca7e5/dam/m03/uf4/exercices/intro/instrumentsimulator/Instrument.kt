package cat.itb.alexhuesca7e5.dam.m03.uf4.exercices.intro.instrumentsimulator

abstract class Instrument(){
    abstract fun makeSounds(times:Int)
}

class Triangle(val resonancia:Int):Instrument(){
    override fun makeSounds(times:Int) {
        when (resonancia){
            1 -> repeat(times){ println("TINC")}
            2 -> repeat(times){ println("TIINC")}
            3 -> repeat(times){ println("TIIINC")}
            4 -> repeat(times){ println("TIIIINC")}
            5 -> repeat(times){ println("TIIIIINC")}
            else -> println("ERROR: Resonancia betweern 1-5")
        }
    }
}
class Drump(val to:String):Instrument(){
    override fun makeSounds(times:Int) {
        when (to){
            "A"-> repeat(times){ println("TAAAM")}
            "O"-> repeat(times){ println("TOOOM")}
            "U"-> repeat(times){ println("TUUUM")}
            else -> println("ERROR: To has to be A,O,U")
        }
    }

}
