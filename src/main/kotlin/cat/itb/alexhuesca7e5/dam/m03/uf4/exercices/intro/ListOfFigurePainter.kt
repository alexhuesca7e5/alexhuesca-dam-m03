package cat.itb.alexhuesca7e5.dam.m03.uf4.exercices.intro

abstract class Figure (var color: String) {
    abstract fun draw()
    fun reset(){
        color = RESET
    }
}
    class RectangleFigure(color:String, private val with:Int, private val height:Int):Figure(color) {

        override fun draw() {
            val color = color
            return repeat(with) {
                repeat(height - 1) { print("$color X") }
                println("$color X")
            }
        }
    }
    class TriangleFigure (color:String, private val height: Int):Figure(color){

        override fun draw() {
            for (i in 1..height) {
                for (j in 1..i) {
                    print("$color X")
                }
                println()
            }
        }
    }
fun main() {
    val firstRectangle= RectangleFigure(RED,4,5)
    val triangle= TriangleFigure(YELLOW,3)
    val thirdRectangle= RectangleFigure(GREEN,3,5)
    firstRectangle.reset()
    firstRectangle.draw()
    triangle.draw()
    thirdRectangle.draw()
    val figuresList = mutableListOf<Figure>(firstRectangle,triangle,thirdRectangle)
    figuresList.forEach { figure -> println(figure.color)}
}