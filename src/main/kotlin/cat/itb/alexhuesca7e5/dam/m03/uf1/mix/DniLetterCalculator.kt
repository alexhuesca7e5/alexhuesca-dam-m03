package cat.itb.alexhuesca7e5.dam.m03.uf1.mix

import java.util.*

fun main() {
    val scn= Scanner(System.`in`)
    val letter= listOf<String>("T","R","W","A","G","M","Y","F","P","D","X","B",
        "N","J","Z","S","Q","V","H","L","C","K","E")
    val dni= scn.next()
    val rest=dni.toInt() % 23
    println("$dni${letter[rest]}")

}


