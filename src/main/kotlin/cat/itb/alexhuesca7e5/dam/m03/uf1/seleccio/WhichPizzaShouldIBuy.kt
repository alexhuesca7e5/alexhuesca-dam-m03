package cat.itb.alexhuesca7e5.dam.m03.uf1.seleccio

import java.util.*

fun pizzes(x:Double,y:Double,c:Double){
    if(Math.PI*((x/2)*(x/2))>y*c){
        println("Compra la rodona")
    }else{
        println("Compra la rectangular")
    }
}
fun main(){
    val scn= Scanner(System.`in`)
    println("Entra el diàmetre de la pizza")
    val x= scn.nextDouble()
    println("Entra els dos costats de la pizza rectangular")
    val y= scn.nextDouble()
    val c= scn.nextDouble()
    pizzes(x, y, c)
}