package cat.itb.alexhuesca7e5.dam.m03.uf5.exercices.regex

import java.util.*

import java.util.*

fun main() {

    val scanner = Scanner(System.`in`)
    var inputValue = scanner.next()

    var code = inputValue.split("-")
    val cicle = code[0]
    var modul = getNumberModul(inputValue)
    var uf = getNumberUF(inputValue)
    println("Estàs cursant la unitat formativa $uf, del mòdul $modul de $cicle.")

}

fun getNumberModul(inputValue:String): Int {
    var regex = Regex("M[0-1][0-9]")
    var modulRegex = regex.find(inputValue)
    println(modulRegex)
    var modulSplit = modulRegex!!.value.split("").filter { it != "" }
    println(modulSplit)
    var numberModul = 0
    if (modulSplit[1].toInt() == 0) {
        numberModul = modulSplit[2].toInt()
    } else {
        numberModul = (modulSplit[1] + modulSplit[2]).toInt()
    }
    return numberModul
}

fun getNumberUF(inputValue:String): Int {
    var regex = Regex("UF[1-6]")
    var ufRegex = regex.find(inputValue)
    var ufSplit = ufRegex!!.value.split("").filter { it != "" }
    return ufSplit[2].toInt()
}
