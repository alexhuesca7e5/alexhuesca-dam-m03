package cat.itb.alexhuesca7e5.dam.m03.uf4.exam.GameEnemiesApp

class Goblins(name:String,lives:Int):Enemy(name, lives){
    override fun attack(strength: Int) {
        if (strength < 4){
            lives-=1
        }
        else{
            lives-=5
        }
        println("L'enemic $name té $lives vides després d'un atac de $strength de força")
    }
}