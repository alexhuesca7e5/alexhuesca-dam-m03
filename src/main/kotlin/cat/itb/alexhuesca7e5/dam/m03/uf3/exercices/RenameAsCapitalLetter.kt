package cat.itb.alexhuesca7e5.dam.m03.uf3.exercices

import java.util.*
import kotlin.io.path.*

fun main() {
    val scn = Scanner(System.`in`)
    val userPath = Path(scn.nextLine())
    userPath.toFile().walk().forEach { file ->
        if (file.isFile){
            val newFile = Path(file.parent, file.name.uppercase(Locale.getDefault()))
            file.renameTo(newFile.toFile())
        }
    }
}