package cat.itb.alexhuesca7e5.dam.m03.uf4.exercices.intro.figures

import cat.itb.alexhuesca7e5.dam.m03.uf4.exercices.intro.GREEN
import cat.itb.alexhuesca7e5.dam.m03.uf4.exercices.intro.RED
import cat.itb.alexhuesca7e5.dam.m03.uf4.exercices.intro.YELLOW
import java.util.*

data class RectangleFigure(val color:String,val with:Int,val height:Int){
    fun createRectangle(){
        val color = color
       return repeat(with) {
           repeat(height - 1) { print("$color X") }
           println("$color X")
       }
    }
}

fun main() {
    val firstRectangle= RectangleFigure(RED,4,5)
    val secondRectangle= RectangleFigure(YELLOW,2,2)
    val thirdRectangle= RectangleFigure(GREEN,3,5)
    firstRectangle.createRectangle()
    secondRectangle.createRectangle()
    thirdRectangle.createRectangle()
}
/*

--TAMBÉ ÉS POT FER AMB UNA MATRIU--

data class RectangleFigure(val color:Color,val with:Int,val height:Int){
    fun createRectangle(){
        val color = color
        val rectangle = List(with){ List(height){"X"}.joinToString(" ") }
        for ( i in 0 ..rectangle.lastIndex){
            println("$color ${rectangle[i]}")
        }
    }
}

--TAMBÉ ÉS POT FER AMB UNA MATRIU I AMB ENUM:--

data class RectangleFigure(val color:Color,val with:Int,val height:Int){
    fun createRectangle(){
        val rectangle = List(with){ List(height){"X"}.joinToString(" ") }
        for ( i in 0 ..rectangle.lastIndex){
            println("${rectangle[i]}")
        }
    }
    fun drawWithColor():String{
    return "{this.color.ref}"+createRectangle()
    }
}
fun main() {
    val firstRectangle= RectangleFigure(Color.RED,4,5)
    val secondRectangle= RectangleFigure(Color.YELLOW,2,2)
    val thirdRectangle= RectangleFigure(Color.GREEN,3,5)
    firstRectangle.drawWithColor()
    secondRectangle.drawWithColor()
    thirdRectangle.drawWithColor()
}
*/