package cat.itb.alexhuesca7e5.dam.m03.uf5.exercices.lists

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val countryList = readCountry(scanner)
    countryList.map { it.nom = it.nom.uppercase(); it.capital = it.capital.uppercase() }
    countryList.forEach { println(it) }
    countryList.forEach{ country ->
        print(country.nom+" ")
        print(country.capital+" ")
        print("${country.superficie} ")
        print("${country.densitat} \n")
    }

    //println(countryList.map { it.nom.uppercase() +" "+ it.capital.uppercase()+" "+it.superficie+" "+it.densitat + "\n" }.joinToString(""))
    /*countryList.forEach { nom ->
        print(nom.nom.uppercase()+" ")
        print(nom.capital.uppercase()+" ")
        print("${nom.superficie} ")
        print("${nom.densitat} \n")
    }

     */
}