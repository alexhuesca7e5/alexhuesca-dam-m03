package cat.itb.alexhuesca7e5.dam.m03.uf2.extra.exercicis

import java.util.Scanner

fun main() {
    val scn = Scanner(System.`in`)
    val size = scn.nextInt()
    val list = mutableListOf<Int>()
    repeat(size) {
        list.add(scn.nextInt())
    }
    val avgCont = list.average()
    var superiorContCount = 0
    for (i in list.indices){
        if (list[i] > avgCont){
            superiorContCount++
        }
    }
    println("Contaminació mitjana: $avgCont")
    println("Dies amb contaminació superior: $superiorContCount")
}