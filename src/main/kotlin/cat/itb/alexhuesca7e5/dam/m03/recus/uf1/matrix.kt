package cat.itb.alexhuesca7e5.dam.m03.recus.uf1

import java.util.Scanner

fun main() {
    val scn = Scanner(System.`in`)
    var rowSize = scn.nextInt()
    var colSize = scn.nextInt()
    val matrix1 = MutableList<MutableList<Int>>(rowSize){ MutableList(colSize){scn.nextInt()} }
    rowSize=scn.nextInt()
    colSize= scn.nextInt()
    val matrix2 = MutableList<MutableList<Int>>(rowSize){ MutableList(colSize){scn.nextInt()} }
    val result = MutableList<MutableList<Int>>(3){ MutableList(3){0}}
    for (i in matrix1.indices){
        for (j in matrix2.indices){
            result[i][j] = matrix1[i][j] + matrix2[i][j]
            //val resultado = matrix1[i][j] + matrix2[i][j]
            //result[i].add(resultado)
        }
    }
    matrix1.forEach(::println)
    matrix2.forEach(::println)
    result.forEach(::println)
}