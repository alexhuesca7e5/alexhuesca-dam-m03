package cat.itb.alexhuesca7e5.dam.m03.uf4.exercices.autonomouscar

interface CarSensors {
    fun isThereSomethingAt(direction: Direction) : Boolean
    fun go(direction : Direction)
    fun stop()
}


