package cat.itb.alexhuesca7e5.dam.m03.uf1.lists

import java.util.*

fun main() {
    val scn=Scanner(System.`in`)
    val listArticles= MutableList(10){scn.nextDouble()}
    for (i in 0..listArticles.lastIndex){
        val result:Double= listArticles[i] * 1.21
        println("${listArticles[i]} IVA= ${"%.2f".format(result)}")
    }

}