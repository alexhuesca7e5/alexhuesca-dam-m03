package cat.itb.alexhuesca7e5.dam.m03.uf1.lists

import java.util.*

fun main() {
    val scn= Scanner(System.`in`)
    val num= scn.nextInt()
    val numbers= List<Int>(num){scn.nextInt()}
    var repeatedNumber=scn.nextInt()
    var text=false
    for (i in numbers){
        if(repeatedNumber==i){
            text=true
            break
        }
    }
    println(text)
}
//IMPORTANT:
//for (i in 0..numbers.lastIndex){SI POSEM 0..NUMBERS.LASTINDEX SIGNIFICA QUE TOTS ELS NUMEROS DE LA TAULA HAN DE SER EL REPEATEDNUMBER PER QUE SIGUIR TRUE
//    if(repeatedNumber==i){
//        text=true
//        break