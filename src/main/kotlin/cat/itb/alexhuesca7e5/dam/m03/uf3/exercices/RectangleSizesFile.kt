package cat.itb.alexhuesca7e5.dam.m03.uf3.exercices

import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import java.io.File
import cat.itb.alexhuesca7e5.dam.m03.uf2.functions.round

@Serializable
data class Color(
    val name: String
)

@Serializable
data class Rectangle(
val color: Color,
val height: Double,
val width: Double
){
    val area = height * width
}

fun main() {
    val jason = File("src/main/resources/cat/itb/alexhuesca7e5/dam/m03/uf3/exercices","rectangle.json")
    val rectangles = Json.decodeFromString<List<Rectangle>>(jason.readText())
    rectangles.forEach { rectangle -> println("Un rectangle de ${rectangle.height} x ${rectangle.width} té ${round(rectangle.area)}  d'area.") }
}