package cat.itb.alexhuesca7e5.dam.m03.uf1.lists

import java.util.*

fun main() {
    val scn = Scanner(System.`in`)
    val candidatos = List<String>(scn.nextInt() + 1) { scn.nextLine() }
    var valor=0
    while(valor!=-1){
        valor=scn.nextInt()
        if(valor==-1)break
        println(candidatos[valor])
    }
}

