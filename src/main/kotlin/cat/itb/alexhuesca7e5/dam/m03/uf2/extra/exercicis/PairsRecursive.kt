package cat.itb.alexhuesca7e5.dam.m03.uf2.extra.exercicis

import java.util.Scanner

fun pairsRecursive(num:Int):Int?{
    if (num == 2){
        return num
    } else
        println(num-2)
    return pairsRecursive(num -2)
}

fun main() {
    val scn = Scanner(System.`in`)
    val num = scn.nextInt()
    pairsRecursive(num)
}