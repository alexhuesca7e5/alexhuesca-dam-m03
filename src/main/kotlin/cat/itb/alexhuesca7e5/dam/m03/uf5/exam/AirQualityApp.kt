package cat.itb.alexhuesca7e5.dam.m03.uf5.exam

import java.util.*

data class AirRecord(val year:Int, val month:Int, val day:Int, val qno2:Double, val qpm10:Double, val qpm25:Double)

val readList :(Scanner) -> List<AirRecord> = { scanner ->
    val size = scanner.nextInt()
    scanner.nextLine()
    val list = mutableListOf<AirRecord>()
    repeat(size){list.add(AirRecord(scanner.nextInt(),scanner.nextInt(),scanner.nextInt(),scanner.nextDouble(),scanner.nextDouble(),scanner.nextDouble()))}
    list
}

fun main() {
    val scan = Scanner(System.`in`).useLocale(Locale.US)
    val list = readList(scan)
    printMaxn02(list)
    printAvPm10(list)
    printDangerousCount(list)
    printWorstno2(list)
}

fun printMaxn02(list:List<AirRecord>){
    val higherno2= list.maxByOrNull { it.qno2 }
    println("Max no2: $higherno2")
}
fun printAvPm10(list:List<AirRecord>){
    val  listAv = mutableListOf<Double>()
    list.forEach{if(it.year>=2021){
        listAv.add(it.qpm10)
    }
    }
    println("Average Pm10: ${listAv.average()}")
}

fun printDangerousCount(list:List<AirRecord>){
    val overpass = list.count{it.qno2>4.0 || it.qpm25>15.0 || it.qpm10>25.0}
    println("Dangerous count: $overpass")
}
fun printWorstno2(list:List<AirRecord>){
    println("Worst no2 2022:")
    list.filter { it.year == 2022 }.sortedByDescending { it.qno2 }.take(3).forEachIndexed{i,airRecord-> println("$i $airRecord")}
}
