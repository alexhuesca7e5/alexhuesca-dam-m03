package cat.itb.alexhuesca7e5.dam.m03.uf2.functions

import java.util.*

fun _readIntList_(scanner: Scanner):List<Int>{
    //val scn= Scanner(System.`in`)
    var inputValue= 0
    val mutableList= mutableListOf<Int>()
    while (true) {
        inputValue = scanner.nextInt()
        if (inputValue==-1) break
        mutableList.add(inputValue)
    }
    return mutableList
}

class minimum(var list: List<Int>){
    fun minimo():Int {
        var minlist= list[0]
        for (value in list){
            if (value<minlist){
                minlist=value
            }
        }
        return minlist
    }
}

fun minimo(list:List<Int>):Int {
    var minlist= list[0]
    for (value in list){
        if (value<minlist){
            minlist=value
        }
    }
    return minlist
}

class maximum(var list: List<Int>){
    fun maxyear():Int {
        var minlist= list[0]
        for (value in list){
            if (value>minlist){
                minlist=value
            }
        }
        return minlist
    }
}

fun maxyear(list: List<Int>):Int {
    var minlist= list[0]
    for (value in list){
        if (value>minlist){
            minlist=value
        }
    }
    return minlist
}

fun averageTemp(list: List<Int>):Double {
    val average= list.average()
    return average
}
