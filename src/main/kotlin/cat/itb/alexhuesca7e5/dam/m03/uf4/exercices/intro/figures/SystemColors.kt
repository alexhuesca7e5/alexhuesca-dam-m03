package cat.itb.alexhuesca7e5.dam.m03.uf4.exercices.intro

// --- Colors for system out ---
// Reset
const val RESET = "\u001b[0m" // Text Reset

// Regular Colors
const val BLACK = "\u001b[0;30m" // BLACK
const val RED = "\u001b[0;31m" // RED
const val GREEN = "\u001b[0;32m" // GREEN
const val YELLOW = "\u001b[0;33m" // YELLOW
const val BLUE = "\u001b[0;34m" // BLUE
const val PURPLE = "\u001b[0;35m" // PURPLE
const val CYAN = "\u001b[0;36m" // CYAN
const val WHITE = "\u001b[0;37m" // WHITE

/*
--OPCIÓ AMB ENUM--
const val RESET = "\u001b[0m"
enum class Color( val ref: String) {
     BLACK ("\u001b[0;30m"),
     RED ("\u001b[0;31m"),
     GREEN ("\u001b[0;32m"),
     YELLOW ("\u001b[0;33m"),
     BLUE ("\u001b[0;34m"),
     PURPLE ("\u001b[0;35m"),
     CYAN ("\u001b[0;36m"),
     WHITE ("\u001b[0;37m")
}
}
 */