package cat.itb.alexhuesca7e5.dam.m03.uf1.iterative

import java.util.*

fun main(){
    val sc= Scanner(System.`in`)
    val inputValue1= sc.nextInt()
    val inputValue2= sc.nextInt()
    var result=1
    var i=0
    while(i<inputValue1){
        result= result * inputValue2
        i++
    }
    println(result)
}