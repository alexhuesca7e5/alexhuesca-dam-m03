package cat.itb.alexhuesca7e5.dam.m03.uf4.exercices.intro

data class BicycleModel (val name:String, val gears:Int, val brand:BycycleBrand){

 data class BycycleBrand(val name:String,val country:String) {
 }
}

fun main() {
    val bici = BicycleModel("Jett 24",5, BicycleModel.BycycleBrand("Specialized","USA"))
    val bici2 = BicycleModel("Hotwalk",7, BicycleModel.BycycleBrand("Specialized","USA"))
    println(bici)
    println(bici2)
}