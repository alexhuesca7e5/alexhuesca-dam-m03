package cat.itb.alexhuesca7e5.dam.m03.uf1.seleccio

import java.util.*

fun main(){
    val scn= Scanner(System.`in`)
    val day= scn.nextInt()
    when(day) {
        1 -> println("Dilluns")
        2 -> println("Dimarts")
        3 -> println("Dimecres")
        4 -> println("Dijous")
        5 -> println("Divendres")
        6 -> println("Dissabte")
        7 -> println("Diumenge")
        else-> println("Error")
    }
}
