package cat.itb.alexhuesca7e5.dam.m03.uf3.exercices

import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.request.*
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

suspend fun main() {

    val client = HttpClient(CIO){
        install(JsonFeature){
            serializer = KotlinxSerializer(kotlinx.serialization.json.Json {
                ignoreUnknownKeys = true
            })
        }
    }
    val temperature : Temperature = client.get("http://api.openweathermap.org/data/2.5/weather?lat=41.390205&lon=2.154007&appid=d662e754d0671e1384f22d2d9023795d")

    val kelvin = temperature.principalData.temperature
    val centigrads = String.format("%.2f", kelvinToCentigrad(kelvin))
    val city = temperature.city
    val humidity = temperature.principalData.humidity


    println("Bon dia,\n" +
            "Avui fa $centigrads º a la ciutat de $city amb una humitat del $humidity %.")
}

fun kelvinToCentigrad(kelvin: Double): Double = kelvin-273.15

@Serializable
data class Temperature(@SerialName("main") var  principalData : PrincipalData, @SerialName("name") var city: String){

}
@Serializable
data class PrincipalData (var humidity:Int, @SerialName("temp") var temperature:Double){

}
