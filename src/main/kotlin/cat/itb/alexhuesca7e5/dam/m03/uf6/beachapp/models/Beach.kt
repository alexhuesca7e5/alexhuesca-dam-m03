package cat.itb.alexhuesca7e5.dam.m03.uf6.beachapp.models

data class Beach(val id: Int, val name: String, val city: String, var quality: Int)