package cat.itb.alexhuesca7e5.dam.m03.uf5.dadescovid

import kotlinx.serialization.Serializable

val listEU = listOf<String>("BE", "EL", "LT", "PT", "BG", "ES", "LU", "RO", "CZ", "FR", "HU", "SI", "DK", "HR", "MT", "SK", "DE", "IT", "NL", "FI", "EE", "CY", "AT", "SE", "IE", "LV", "PL")
@Serializable
data class CovidData(
    val Countries: List<Country>,
    val Date: String,
    val Global: Global,
    val ID: String,
    val Message: String
){
    fun printtop10Deaths(){
        val countries = Countries.sortedByDescending{ it.TotalDeaths }
        println(
        countries.joinToString (separator = "\n", limit = 10, prefix = "#### Most deaths: #### \n"){ country ->  "${countries.indexOf(country)+1}.${country.Country}: ${country.TotalDeaths}" }
        )
    }
    fun printTop10Confirmed(){
        val countries = Countries.sortedByDescending{ it.NewConfirmed }
        println(
        countries.joinToString (separator = "\n", limit = 10, prefix = "#### New confirmed ####\n"){ country ->  "${countries.indexOf(country)+1}.${country.Country}: ${country.NewConfirmed}" }
        )
    }
    fun printTopEU(){
        val conutriesEU = Countries.filter { country -> country.CountryCode in listEU  }
        val totalDeaths = conutriesEU.sumOf { it.TotalDeaths }
        val newConfirmed = conutriesEU.sumOf { it.NewConfirmed }
        println(
            conutriesEU.joinToString (separator = "\n", limit = 1, prefix = "#### EU data ####\n"){ "Total Deaths: $totalDeaths \nNew Confirmed: $newConfirmed" }
        )
    }
    fun printSpainDeaths(){
        var spainDeaths= 0.0
        var spainCases= 0.0
        Countries.filter { it.CountryCode == "ES" }.forEach { spainDeaths += it.TotalDeaths.toDouble()}
        Countries.filter { it.CountryCode == "ES" }.forEach { spainCases += it.TotalConfirmed.toDouble()}
        println("### Spain Relative ###\n"+"#### SPAIN ####\n"+"Deaths per capita: ${(spainDeaths/spainCases)*100}")
    }
}

