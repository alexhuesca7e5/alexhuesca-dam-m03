package cat.itb.alexhuesca7e5.dam.m03.uf3.exercices

import java.nio.file.StandardOpenOption
import java.time.LocalDateTime
import kotlin.io.path.Path
import kotlin.io.path.writeText

fun main() {
    val path = Path("/home/sjo/Escriptori/DADES/Alex_Huesca/M03/alexhuesca-dam-m03/src/main/resources/cat/itb/alexhuesca7e5/dam/m03/uf3/exercices","i_was_here.txt")
    val date = LocalDateTime.now().toString()
    val text = "I Was Here: $date \n"
    path.writeText(text, options = arrayOf(StandardOpenOption.APPEND))
}