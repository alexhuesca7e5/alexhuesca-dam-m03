package cat.itb.alexhuesca7e5.dam.m03.uf3.exercices.generalexam

import cat.itb.alexhuesca7e5.dam.m03.uf2.functions.round
import kotlin.io.path.Path
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.util.*
import kotlin.io.path.readText
import kotlin.io.path.writeText

@Serializable
class FoodTacker(val name:String, val grams:Int,val kcalGram:Double) {
    val kcalTotal = grams* kcalGram
}

fun main() {
    val scn = Scanner(System.`in`).useLocale(Locale.UK)
    /*
    He creat l'arxiu "food.json" a la carpeta resources"
     */
    val json = Path("src/main/resources","food.json")
    val foodIfo = FoodTacker(scn.next(),scn.nextInt(),scn.nextDouble())
    val foodList: MutableList<FoodTacker> = Json.decodeFromString(json.readText())
    foodList.add(foodIfo)
    json.writeText(Json.encodeToString(foodList))
    foodList.forEach { food ->
        println("${food.name} ${food.grams}g, ${round(food.kcalTotal)} kcal")
    }
}