package cat.itb.alexhuesca7e5.dam.m03.uf1.extra.exercicis

import java.util.Scanner

fun main() {
    val scn = Scanner(System.`in`)
    var checker:Boolean? = null
    val checkArray= mutableListOf<Boolean>()
    val result = mutableListOf<String>()
    var diviend:Int=0
    var divisor:Int= 0
    var quocient = 0
    var residu = 0
    while (true){
        diviend = scn.nextInt()
        if (diviend == -1) break
        divisor = scn.nextInt()
        quocient = scn.nextInt()
        residu = scn.nextInt()
        checker = (((divisor*quocient)+residu) == diviend)
        checkArray.add(checker)
    }
    for (i in checkArray){
        if(i==true){
            result.add("correcte")
        } else result.add("incorrecte")
    }
    for (i in result){
        println(i)
    }
}

