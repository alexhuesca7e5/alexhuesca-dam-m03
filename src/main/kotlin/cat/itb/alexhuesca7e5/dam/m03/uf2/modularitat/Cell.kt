package cat.itb.alexhuesca7e5.dam.m03.uf2.modularitat

data class Cell(var positionI:Int,
var positionJ:Int,
var containsBomb:Boolean=false,
var uncovered:Boolean=false,
){
    var adjacency:Int=-1

    fun showInformation(): String {
        return if (!uncovered){
            "X"
        } else if (uncovered && containsBomb){
            "B"
        } else if (uncovered){
            adjacency.toString()
        }else "Type again"

    }

}
