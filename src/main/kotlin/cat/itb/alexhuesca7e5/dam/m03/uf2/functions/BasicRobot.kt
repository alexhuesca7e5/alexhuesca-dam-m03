package cat.itb.alexhuesca7e5.dam.m03.uf2.functions

import java.util.*


data class RobotPositionAndVel(
    var position: Position = Position(0.0,0.0),
    var speed:Double=1.0){
    data class Position(var X:Double ,var Y:Double)
    fun robot(action:String){
        when(action.uppercase()){
            "DALT" -> goUp()
            "BAIX" -> doDown()
            "DRETA" -> right()
            "ESQUERRA"-> left()
            "ACCELERAR"-> speedUp()
            "DISMINUIR"-> speedDown()
            "POSICIO"-> currentPosition()
             "VELOCITAT"-> currentSpeed()
        }
        }

    private fun currentSpeed() {
        println("La velocitat del robo és $speed")
    }

    private fun currentPosition() {
        println("La posició del robot és (${position.X}, ${position.Y})")
    }

    private fun speedDown() {
        if (speed!=0.0){
            speed-=0.5
    }
}

    private fun speedUp() {
        if (speed!=10.0){
            speed+=0.5}
    }

    private fun left() {
        position.X-=speed
    }

    private fun right() {
        position.X+=speed
    }

    private fun doDown() {
        position.Y-=speed
    }

    private fun goUp() {
        position.Y+=speed
    }
}

fun main() {
    val scn=Scanner(System.`in`)
    var inputValue:String=""
    var basicRobot=RobotPositionAndVel()
    while(true){
        inputValue=scn.nextLine()
        if (inputValue=="END")break
        basicRobot.robot(inputValue)
    }

}
