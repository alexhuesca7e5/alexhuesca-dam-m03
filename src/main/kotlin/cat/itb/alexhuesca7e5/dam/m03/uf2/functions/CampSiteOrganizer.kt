package cat.itb.alexhuesca7e5.dam.m03.uf2.functions

import java.util.*

data class Plot(val numPers:Int, val ploatName:String)
data class CampSiteOrganizer(var reservations: MutableList<Plot> = mutableListOf()){
        fun plotUpdate (action:String, nomPersInPlot:Int, namePloat:String){
            when(action){
                "ENTRA"-> reservations.add(Plot(nomPersInPlot,namePloat))
                "MARXA"-> reservations.removeIf{plot -> plot.ploatName == namePloat}
                }
            }
        }

fun main() {
    val scn=Scanner(System.`in`)
    var inputValue:String=""
    var caio= CampSiteOrganizer()
    while (true){
        inputValue=scn.next()
        if (inputValue == "END") break
        when(inputValue){
            "ENTRA"->caio.plotUpdate(action = inputValue,scn.nextInt(),scn.next())
            "MARXA"->caio.plotUpdate(action = inputValue,0,scn.next())
        }
        println("parcel·les: ${caio.reservations.lastIndex+1}\n" +
                "persones: ${caio.reservations.sumOf { it.numPers }}")

    }

}