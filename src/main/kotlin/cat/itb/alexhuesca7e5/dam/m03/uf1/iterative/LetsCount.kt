package cat.itb.alexhuesca7e5.dam.m03.uf1.iterative

import java.util.*

fun main(){
    val scn=Scanner(System.`in`)
    val times=scn.nextInt()
    repeat(times){
        print(it+1)
    }
}