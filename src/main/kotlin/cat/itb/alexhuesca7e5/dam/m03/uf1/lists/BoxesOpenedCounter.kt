package cat.itb.alexhuesca7e5.dam.m03.uf1.lists

import java.util.*

fun main() {
    val scn = Scanner(System.`in`)
    val box = MutableList(11) {0}
    var num = scn.nextInt()
    while (num != -1) {
        box[num] = box[num]+1
        num = scn.nextInt()
    }
    println(box)
}
