package cat.itb.alexhuesca7e5.dam.m03.uf1.seleccio

import java.util.*
import kotlin.math.roundToInt

fun main() {
    val scn = Scanner(System.`in`)
    var euroUsuari = scn.nextDouble()

    if (euroUsuari >= 500) {
       val cincents = euroUsuari / 500
        euroUsuari %=500
        println("${cincents.toInt()} bitllets de cincents")
    }
    if (euroUsuari >= 200) {
        val doscents = euroUsuari / 200
        euroUsuari %=200
        println("${doscents.toInt()} bitllets de doscents")
    }
    if (euroUsuari>=100){
        val cent= euroUsuari/100
        euroUsuari%=100
        println("${cent.toInt()} bitllets de cent")
    }
    if (euroUsuari>=50){
        val cincq= euroUsuari/50
        euroUsuari%=50
        println("${cincq.toInt()} bitletts de cinquanta")
    }
    if(euroUsuari>=20){
        val vint= euroUsuari/20
        euroUsuari%=20
        println("${vint.toInt()} bitllets de vint")
    }
    if(euroUsuari>=10){
        val deu= euroUsuari/10
        euroUsuari%=10
        print("${deu.toInt()} bitllets de deu")
    }
    if(euroUsuari>=5){
        val cinc= euroUsuari/5
       euroUsuari%=5
        println("${cinc.toInt()} bitllets de cinc")
    }
    if(euroUsuari>=2){
       val dos= euroUsuari/2
        euroUsuari%=2
        println("${dos.toInt()} monedes de dos")
    }
    if(euroUsuari>=1){
       val un= euroUsuari/1
        euroUsuari%=1
        println("${un.toInt()} monedes d'1 euro")
    }
    var centimosusuari= (euroUsuari*100).roundToInt()
    if(centimosusuari>=50){
        val cinqcent=centimosusuari/50
        centimosusuari%=50
        println("$cinqcent monedes de cinquanta centims")
    }
    if(centimosusuari>=20){
       val vintCentims=centimosusuari/20
        centimosusuari%=20
        println("$vintCentims monedes de vint centims")
    }
    if(centimosusuari>=10){
        val deuCentims=centimosusuari/10
        centimosusuari%=10
        println("$deuCentims monedes de deu centims")
    }
    if(centimosusuari>=5){
       val cincCentims=centimosusuari/5
        centimosusuari%=5
        println("$cincCentims monedes de cinc centims")
    }
    if(centimosusuari>=2) {
        val dosCentims = centimosusuari / 2
         centimosusuari%=2
        println("$dosCentims monedes de dos centims")
    }
    if(centimosusuari>=1){
        println("1 moneda d'1 centim")}
}


