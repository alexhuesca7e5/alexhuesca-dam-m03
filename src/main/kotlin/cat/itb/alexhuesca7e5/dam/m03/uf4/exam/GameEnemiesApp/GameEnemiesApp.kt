package cat.itb.alexhuesca7e5.dam.m03.uf4.exam.GameEnemiesApp

fun main() {
    val zombie1 = Zombies("Zog",10,"AARRRrrgg")
    val zombie2 = Zombies("Lili",30,"GRAaaArg")
    val troll = Trolls("Jum",12,5)
    val goblin = Goblins("Tim",60)
    val list = listOf<Enemy>(zombie1,zombie2,troll,goblin)
    zombie1.attack(5)
    zombie1.attack(7)
    goblin.attack(7)
    zombie2.attack(3)
    troll.attack(4)
    troll.attack(8)
    zombie2.attack(4)
    troll.attack(1)
    troll.attack(1)
    troll.attack(1)
    println(list)
}