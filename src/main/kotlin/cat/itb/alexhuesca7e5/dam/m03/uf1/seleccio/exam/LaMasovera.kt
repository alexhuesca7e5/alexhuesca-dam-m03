package cat.itb.alexhuesca7e5.dam.m03.uf1.seleccio.exam

import java.util.*

fun main(){
    println("Escriu el dia de la setmana en lletres:")
    val scn= Scanner(System.`in`)
    when (scn.next()){
        "dilluns"->println("Compra llums")
        "dimarts"-> println("Compra naps")
        "dimecres"->println("Compra nespres")
        "dijous"-> println("Compra nous")
        "divendres"->println("Faves tendres")
        "dissabte"->println("Tot s'ho gasta")
        "diumenge"->println("Tot s'ho menja")
    }
}