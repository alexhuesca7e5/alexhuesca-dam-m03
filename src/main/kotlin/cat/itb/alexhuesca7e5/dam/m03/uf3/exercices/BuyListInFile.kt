package cat.itb.alexhuesca7e5.dam.m03.uf3.exercices

import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.nio.file.Path
import java.util.*
import kotlin.io.path.Path
import kotlin.io.path.readText
import kotlin.io.path.writeText

@Serializable
data class BuyListInFile (val quantity: Int, val product:String, val price:Double)

fun main() {
    val scn = Scanner(System.`in`).useLocale(Locale.UK)
    val jason = Path("src/main/resources/cat/itb/alexhuesca7e5/dam/m03/uf3/exercices","productList.json")
    val buyList = BuyListInFile(scn.nextInt(),scn.next(), scn.nextDouble())
    if (jason.readText() == ""){
        jason.writeText("[]")
    }
   val productsList:MutableList<BuyListInFile> = Json.decodeFromString(jason.readText())
    productsList.add(buyList)
    jason.writeText(Json.encodeToString(productsList))

    println(
        "-------- Compra --------")

    productsList.forEach { product ->
        println("${product.quantity} ${product.product} (${product.price}) - ${product.quantity * product.price}€\n"
                ) }
    println( "-------------------------\n"+
            "Total: ${productsList.sumOf { it.quantity*it.price }}€theme\n"+
            "-------------------------")
}
/*
@Serializable
class Productes (val quantitat: Int,val nom:String,val preu:Double,){
    val preuPerQuantitat get() = preu * quantitat
}
--OPCIÓ B--

fun main() {
    val scan = Scanner(System.`in`).useLocale(Locale.UK)
    val path = Path ("src/main/resources/cat/itb/alexhuesca7e5/dam/m03/uf3/exercices","productes.json")
    val list = Productes(scan.nextInt(),scan.next(),scan.nextDouble())
    val json:MutableList<Productes> = Json.decodeFromString(path.readText())
    json.add(list)
    path.writeText(Json.encodeToString(json))
    println(
        "-------- Compra --------"
    )
    json.forEach { producte ->
        println("${producte.quantitat} ${producte.nom} (${producte.preu}) - ${producte.preuPerQuantitat}")
        }
        println("-------------------------\n"+
                "Total: ${json.sumOf { Productes -> Productes.preuPerQuantitat }}\n"+
                "-------------------------")
}
 */