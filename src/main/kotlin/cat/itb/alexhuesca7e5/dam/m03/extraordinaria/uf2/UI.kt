package cat.itb.alexhuesca7e5.dam.m03.extraordinaria.uf2

import java.util.Scanner

class UI (val scn: Scanner = Scanner(System.`in`), val animalApp:AnimalAPP){
    fun start(){
        println("Introduceix els animals")
        val list = animalApp.readAnimalList(scn)
        animalApp.printAnimals(list)
        println("Indica un tipus d'animal per filtrar")
        val animal = scn.next()
        animalApp.filterAnimal(animal).forEach { println(it) }
        println("Indica l'identificador d'un animal per saber de quin tipus és")
        val identificador = scn.next()
        println("$identificador és un ${animalApp.getAnimalById(identificador)}")
        println("Indica l'identificador d'un animal per saber-ne els detalls")
        val ident = scn.next()
        println(animalApp.getAnimalDataById(ident))
        println("Indica un tipus d'animal per saber-ne el resum")
        val animals = scn.next()
        println("---------- Resum ----------")
        animalApp.filterAnimal(animals).forEach { an ->
            println("Total: ${an.total}\n"+
                    "Més golós: ${list.maxByOrNull { it.total }}")}
    }
}
class AnimalAPP(val list: MutableList<Animal>) {

    fun readAnimalList(scn: Scanner): List<Animal> {
        val size = scn.nextInt()
        repeat(size) {
            list.add(Animal(scn.next(), scn.next(), scn.next(), scn.nextInt(), scn.nextInt()))
        }
        return list
    }

    fun printAnimals(list: List<Animal>) {
        println("---------- Animals ----------")
        list.forEach { animal ->
            println("${animal.codi}-${animal.nom}: ${animal.numApats} àpats, ${animal.quantMenjar}g/apat, total ${animal.total}g")
        }
        println("---------- Resum ----------")
        println("Total: ${list.sumOf { it.total }}\n" +
                "Més golós: ${list.maxByOrNull { it.total }}"
        )
    }
    fun filterAnimal(animalName: String): List<Animal> {
       return list.filter { it.tipus == animalName }
    }
    fun getAnimalById(id:String): String? {
        return list.find { it.codi == id }?.tipus
    }

    fun getAnimalDataById(id:String): Animal? {
        return list.find { it.codi == id }
    }
}