package cat.itb.alexhuesca7e5.dam.m03.uf2.recursivity

import java.util.*

fun multiplica(a:Int,b:Int): Int{
    if ( b == 0)
        return 0
    else
        return a + multiplica(a,b-1)
}

fun main() {
    val scn= Scanner(System.`in`)
    val numberOne = scn.nextInt()
    val numberTwo = scn.nextInt()
    println(multiplica(numberOne,numberTwo))
}