package cat.itb.alexhuesca7e5.dam.m03.uf5.exercices.lists

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val integerList = readCountry(scanner)
    val kmCountries = integerList.filter { country -> country.superficie < 1200000}.sumOf { (it.densitat*it.superficie)/integerList.size }
    println("La mitjana de poblacó és de $kmCountries")
}