package cat.itb.alexhuesca7e5.dam.m03.uf2.functions

import cat.itb.alexhuesca7e5.dam.m03.uf1.lists.exam.readIntList
import java.util.*

fun main() {
    val scanner= Scanner(System.`in`)
    val x= minimum(_readIntList_(scanner))
    val minim= x.minimo()
    println("El producte més econòmic val: ${minim}")
}