package cat.itb.alexhuesca7e5.dam.m03.uf4.exam.GameEnemiesApp

data class Trolls(override var name:String, override var lives:Int, val resistencia:Int):Enemy(name,lives){
    override fun attack(strength: Int) {
        if(resistencia < strength){
            lives-=1
        }
        else{
            lives = lives
        }
        println("L'enemic $name té $lives vides després d'un atac de $strength de força")
    }
}