package cat.itb.alexhuesca7e5.dam.m03.uf4.exam.GameEnemiesApp

data class Zombies(override var name:String, override var lives:Int, val so:String):Enemy(name, lives){
    override fun attack(strength: Int) {
        lives-=strength
        if (lives<=0){
            println("L'enemic $name ja està mort")
        }
        else{
            println(so)
            println("L'enemic $name té $lives vides després d'un atac de $strength de força")
        }
    }

}
