package cat.itb.alexhuesca7e5.dam.m03.uf1.lists

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.US)
    val matrixOne = scannerReadIntMatrix(scanner)
    val matrixTwo = scannerReadIntMatrix(scanner)
    val matrixSum= MutableList(matrixOne.size){MutableList(matrixTwo.first().size){0}}
    for(i in matrixOne.indices){
        for (j in matrixOne[i].indices){
           val sum= matrixOne[i][j]+matrixTwo[i][j]
            matrixSum[i][j]+=sum
        }
    }
    for(i in matrixSum){
        println(i)
    }
}