package cat.itb.alexhuesca7e5.dam.m03.uf3.exercices

import cat.itb.alexhuesca7e5.dam.m03.uf2.functions.round
import java.util.*
import kotlin.io.path.Path
import kotlin.io.path.readLines
import kotlin.io.path.readText

fun main() {
    val scn = Scanner(System.`in`)
    val userPath = "/dades/dades/Alex_Huesca"
    val search = Path(userPath)
    val file = "RadarFile.txt"
    val userFile = Path("$search/$file").toFile()
    val intlist : List<Int> = userFile.readText().map { it.code }
    val max: Int = intlist.maxOrNull() ?: 0
    val min: Int = intlist.minOrNull() ?: 0
    val average: Double = intlist.average()
    println("Velocitat màxima: $max km/h")
    println("Velocitat mínima: $min km/h")
    println("Velocitat mitjana: ${round(average)} km/h")
}

fun round(num:Double): String {
    return String.format("%.2f",num)
}