package cat.itb.alexhuesca7e5.dam.m03.uf4.exam.magazine

abstract class Article():Printable {
    abstract val title: String
    abstract val autor: String
}

data class Opinion(override val title:String, override val autor:String, val text:String):Article(){
    override fun print(magazinePrinter: MagazinePrinter) {
        val printer = DummyMagazinePrinter()
        printer.printTitle("Opinió: $title", autor)
        printer.printText(text)
    }
}
data class Fotografics(override val title: String, override val autor: String, val list:List<String>):Article(){
    override fun print(magazinePrinter: MagazinePrinter) {
        val printer = DummyMagazinePrinter()
        printer.printTitle("Fotos: $title", autor)
        printer.printPhotos(list)
    }

}

