package cat.itb.alexhuesca7e5.dam.m03.uf4.exercices.intro

open class Person(val name: String, val birthDate: String){
    open fun whatAmI() = " I'm a person"
}
class Student(name: String, birthDate: String, val startYear: Int) : Person(name, birthDate){
    override fun whatAmI():String = "I'm a student" + super.whatAmI()
}

fun main() {
    val student = Student("Alex", "28/02/2001", 2001)
    println(student.whatAmI().toString())
}

/*
Per poder printejar una classe amb herència sense utilitzqr data class (ja que no ens deixa), hem de sobreesriure (overrride)
la funció per tal de passar-la a string per tal de que funcioni.
 */