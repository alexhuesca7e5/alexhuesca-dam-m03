package cat.itb.alexhuesca7e5.dam.m03.recus.uf1

import java.util.*

fun main() {
    val scn = Scanner(System.`in`)
    val matrix1 = MutableList<MutableList<Int>>(3){ MutableList(3){scn.nextInt()} }
    var totalSum = 0
    for (i in matrix1.indices){
        totalSum += matrix1[i][i]
    }
    println("Sum: $totalSum")
}