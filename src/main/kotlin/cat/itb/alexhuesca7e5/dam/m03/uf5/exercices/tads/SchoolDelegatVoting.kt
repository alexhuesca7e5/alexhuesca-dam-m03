package cat.itb.alexhuesca7e5.dam.m03.uf5.exercices.tads

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    //var votes = readVotes(scanner)
    var votes2 = readVotes2(scanner)
    println(votes2)
//    votes.forEach{
//        println("${it.key} : ${it.value}")
//    }
}

fun readVotes2(scanner: Scanner): Map<String,Int> {
    var votes = mutableListOf<String>()
    var vote = scanner.nextLine()
    while ( vote != "END"){
        votes.add(vote)
        vote = scanner.nextLine()
    }
    return votes.groupingBy { it }.eachCount()
}

fun readVotes(scanner: Scanner): MutableMap<String, Int> {
    var votes = mutableMapOf<String,Int>()
    var vote = scanner.nextLine()
    while ( vote != "END"){
        if (votes.containsKey(vote)){
            //votes.set(vote, votes.get(vote)!!+1)
            votes[vote] = votes[vote]!! + 1
        }
        else //votes.put(vote,1)
            votes[vote]=1
        vote = scanner.nextLine()
    }
    return votes
}
