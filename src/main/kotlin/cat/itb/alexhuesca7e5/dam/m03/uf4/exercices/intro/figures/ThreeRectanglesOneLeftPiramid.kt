package cat.itb.alexhuesca7e5.dam.m03.uf4.exercices.intro.figures

import cat.itb.alexhuesca7e5.dam.m03.uf4.exercices.intro.GREEN
import cat.itb.alexhuesca7e5.dam.m03.uf4.exercices.intro.RED
import cat.itb.alexhuesca7e5.dam.m03.uf4.exercices.intro.YELLOW

open class Figure (val color: String){
    class RectangleFigure(color:String, private val with:Int, private val height:Int):Figure(color){
        fun createRectangle(){
            val color = color
            return repeat(with) {
                repeat(height - 1) { print("$color X") }
                println("$color X")
            }
        }
    }
    class TriangleFigure (color:String, private val height: Int):Figure(color){
        fun createTriangle(){
            for (i in 1..height) {
                for (j in 1..i) {
                    print("$color X")
                }
                println()
            }
        }
    }
}
fun main() {
    val firstRectangle= Figure.RectangleFigure(RED,4,5)
    val triangle= Figure.TriangleFigure(YELLOW,3)
    val thirdRectangle= Figure.RectangleFigure(GREEN,3,5)
    firstRectangle.createRectangle()
    triangle.createTriangle()
    thirdRectangle.createRectangle()
}