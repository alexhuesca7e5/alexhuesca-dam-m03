package cat.itb.alexhuesca7e5.dam.m03.uf1.iterative

import java.util.*

fun main() {
    val sc= Scanner(System.`in`)
    val blankSpaces= sc.nextInt()
    val lengthLine= sc.nextInt()
    repeat(lengthLine) {
        repeat(blankSpaces) {print(" ")}
        println("*")
    }}
