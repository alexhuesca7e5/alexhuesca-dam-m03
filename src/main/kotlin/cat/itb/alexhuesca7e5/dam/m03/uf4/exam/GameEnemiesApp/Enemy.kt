package cat.itb.alexhuesca7e5.dam.m03.uf4.exam.GameEnemiesApp

abstract class Enemy(open val name:String, open var lives:Int){
    abstract fun attack(strength:Int)
}

