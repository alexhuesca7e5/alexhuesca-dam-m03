package cat.itb.alexhuesca7e5.dam.m03.uf2.generalexam

import java.util.*

fun countOnesRec(num:Int, count:Int = 0):Int {
    return if (num == 0) return 0
    else if (num % 10 == 1) {
        count+1 + countOnesRec(num/10)
    } else {
        return count + countOnesRec(num/10)
    }
}
fun main() {
    val scn = Scanner(System.`in`)
    val number = scn.nextInt()
    println(countOnesRec(number))
}