package cat.itb.alexhuesca7e5.dam.m03.uf4.exercices.intro

class Board (val list: List<Rectangle>){

 fun getTotalArea(): Double {
  var area = 0.0
  for (rectangle in this.list) {
   area += rectangle.area
  }
  return area
 }

 fun countRectangles(): Int {
  return this.list.size
 }

}
class Rectangle(val height:Double, val width:Double) {
 val area get() = height * width
}

fun main() {
 val rectangles = Board(listOf(Rectangle(2.2, 4.5), Rectangle(3.4, 1.5)))
 println(rectangles.countRectangles())
 println(rectangles.getTotalArea())
}