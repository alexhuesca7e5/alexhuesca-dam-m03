/**
package cat.itb.alexhuesca7e5.dam.m03.uf3.exercices

import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.request.*
import kotlinx.serialization.*
import java.net.http.HttpClient

suspend fun main() {

    val client = HttpClient(CIO){
        install(JsonFeature){
            serializer = KotlinxSerializer(kotlinx.serialization.json.Json {
                ignoreUnknownKeys = true
            })
        }
    }
    val famous : Famous = client.get("https://api.quotable.io/random")


    val famousAuthor = famous.author
    val content = famous.content


    println("$famousAuthor:\n" +
            content
    )
}

@Serializable
data class Famous(@SerialName("author") var  author : String, @SerialName("content") var content: String){

}
**/

