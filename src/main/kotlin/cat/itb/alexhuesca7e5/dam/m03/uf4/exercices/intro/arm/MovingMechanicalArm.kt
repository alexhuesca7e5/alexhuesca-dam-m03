package cat.itb.alexhuesca7e5.dam.m03.uf4.exercices.intro.arm

import cat.itb.alexhuesca7e5.dam.m03.uf4.exercices.intro.MechanicalArm

class MovingMechanicalArm(openAngle:Double = 0.0, altitude: Double = 0.0,turnedOn: Boolean = false , var move:Double = 0.0):
    MechanicalArm(openAngle,altitude,turnedOn){
    fun moveArm (movement:Double){
        this.move =+ movement
        if (move < 0.0) {
            move = 0.0
        }
        println(this)
    }
    override fun toString(): String {
        return "openAngle:$openAnlge, altitude:$altitude, turnedOn:$turnedOn, move:$move"
    }
}