package cat.itb.alexhuesca7e5.dam.m03.uf2.functions

import java.util.*

data class MatchAnalizer(var team:String, var points:Int){
    fun punctuation(updatePoints:Int):String?{
        val result= when(updatePoints-points){
            1-> ("Tir lliure de $team")
            2-> ("Cistella de $team")
            3-> ("Triple de $team")
            else-> null
        }
        if (result!=null){
            points=updatePoints

        }
        return result
    }
}
fun main() {
    val scn=Scanner(System.`in`)
    val team1= MatchAnalizer(scn.next(),0)
    val team2= MatchAnalizer(scn.next(),0)
    val pointsTeam1= team1.points
    val pointsTeam2= team2.points
    var updatePointsTeam1=0
    var updatePointsTeam2=0
    val list= mutableListOf<String?>()
    while (true){
        updatePointsTeam1= scn.nextInt()
        if (updatePointsTeam1==-1) break
        updatePointsTeam2= scn.nextInt()
        if (updatePointsTeam2==-1)break
        val analizeTeam1= team1.punctuation(updatePointsTeam1)
        list.add(analizeTeam1)
        val analizeTeam2= team2.punctuation(updatePointsTeam2)
        list.add(analizeTeam2)
    }
    for(value in list){
        if (value!=null){
            println(value)
        }
    }
    when{
        team1.points>team2.points-> println("Guanya ${team1.team}")
        team1.points<team2.points-> println("Guanya ${team2.team}")
        else-> println("Empat")
    }
}