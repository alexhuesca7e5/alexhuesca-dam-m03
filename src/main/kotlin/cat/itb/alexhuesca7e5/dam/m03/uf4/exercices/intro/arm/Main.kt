package cat.itb.alexhuesca7e5.dam.m03.uf4.exercices.intro.arm

import cat.itb.alexhuesca7e5.dam.m03.uf4.exercices.intro.MechanicalArm
import cat.itb.alexhuesca7e5.dam.m03.uf4.exercices.intro.arm.MovingMechanicalArm as MovingMechanicalArm

fun main() {
    val arm = MechanicalArm()
    val move = MovingMechanicalArm()
    arm.toggle()
    arm.updateAltitude(3.0)
    move.moveArm(4.5)
    arm.updateAngle(180.0)
    arm.updateAltitude(-3.0)
    arm.updateAngle(-180.0)
    arm.updateAltitude(3.0)
    move.moveArm(-4.5)
    arm.toggle()
}