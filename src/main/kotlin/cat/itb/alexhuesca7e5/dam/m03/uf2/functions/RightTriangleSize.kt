package cat.itb.alexhuesca7e5.dam.m03.uf2.functions

import java.util.*
import kotlin.math.sqrt

data class RightTriangleSize(var base:Double, var altura:Double){
    fun area(): Double {
        val areaTriangle= (base * altura) / 2
        return areaTriangle
    }
    fun perimeter(): Double {
        val perimeterTriangle= (base+altura)+(sqrt(base*base+ altura*altura))
        return perimeterTriangle
    }
}

fun main() {
    val scn = Scanner(System.`in`).useLocale(Locale.UK)
    val triangles= List(scn.nextInt()){RightTriangleSize(scn.nextDouble(),scn.nextDouble())}
    triangles.forEach {
        println("Un triangle de ${round(it.base)} x ${round(it.altura)} té ${round(it.area())} d'area i ${round(it.perimeter())} de perímetre")
    }
}
fun round(num:Double): String {
    return String.format("%.2f",num)
}