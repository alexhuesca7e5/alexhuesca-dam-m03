package cat.itb.alexhuesca7e5.dam.m03.uf1.lists

import java.util.*

fun main() {
    val scn= Scanner(System.`in`)
    val strangeOrder= mutableListOf<Int>()
    while(true){
        var number=scn.nextInt()
        if(number ==-1){ break}
        strangeOrder.add(0,number)
        var numberTwo=scn.nextInt()
        if(numberTwo == -1){break}
        strangeOrder.add(numberTwo)
    }
    println(strangeOrder)
}
