package cat.itb.alexhuesca7e5.dam.m03.uf3.exercices

import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import java.io.File
import java.util.*

@Serializable
data class ArbresItem(
    val adreca: String?,
    val cat_especie_id: Int?,
    val catalogacio: String?,
    val categoria_arbrat: String?,
    val codi: String?,
    val codi_barri: String?,
    val codi_districte: String?,
    val data_plantacio: String?,
    val espai_verd: String?,
    val geom: String?,
    val latitud: String?,
    val longitud: String?,
    val nom_barri: String?,
    val nom_castella: String?,
    val nom_catala: String?,
    val nom_cientific: String?,
    val nom_districte: String?,
    val tipus_aigua: String?,
    val tipus_element: String?,
    val tipus_reg: String?,
    val x_etrs89: String?,
    val y_etrs89: String?
)

fun main() {
    val json = File("src/main/resources/cat/itb/alexhuesca7e5/dam/m03/uf3/exercices","arbres.json")
    val trees = Json.decodeFromString<List<ArbresItem>>(json.readText())
    val scn = Scanner(System.`in`)
    val treeName = scn.nextLine()
    val numberOfTrees = trees.count { tree -> tree.nom_cientific == treeName }
    println(numberOfTrees)
}