package cat.itb.alexhuesca7e5.dam.m03.uf4.exercices.motto


fun main() {
    val teams = listOf(
        Team("Mosques", "Bzzzanyarem"),
        Team("Dragons", "Grooarg"),
        Team("Abelles", "Piquem Fort"))
    shoutMottos(teams)
}

fun shoutMottos(teams: List<Team>){
    for(team in teams)
        team.shoutMotto()
}
