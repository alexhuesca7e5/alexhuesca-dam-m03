package cat.itb.alexhuesca7e5.dam.m03.uf1.lists.exam

import java.util.*

fun main() {
    val list= readIntList(Scanner(System.`in`))
    val positiveNumbers= mutableListOf<Int>()
    for (i in list.indices){
        if(list[i]>0){
            positiveNumbers.add(list[i])
        }
    }
    println(positiveNumbers.sum())
}
fun readIntList(scanner: Scanner): List<Int> {
    return List(scanner.nextInt()) { scanner.nextInt() }
}