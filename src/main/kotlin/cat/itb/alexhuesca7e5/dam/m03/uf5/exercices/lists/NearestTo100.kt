package cat.itb.alexhuesca7e5.dam.m03.uf5.exercices.lists

import java.lang.Math.abs

fun main() {
    val integerList = listOf<Int>(971,6,4878,1354,102,302154,-2)
    val closestNumber = integerList.sortedBy { abs(it-100) }.first()
    val farestNumber = integerList.sortedBy { abs(it-100) }.last()
    println("Més proper a 100: $closestNumber")
    println("Més llunyà a 100: $farestNumber")
}