package cat.itb.alexhuesca7e5.dam.m03.uf5.exercices.tads

import java.util.*

fun main() {

    val scanner = Scanner(System.`in`)
    var employees = readEmployee(scanner)
    resolveEmployeeByDNI(scanner,employees )
}

fun resolveEmployeeByDNI(scanner: Scanner, employeeMap: MutableMap<String, Empleat>) {
    var dni = scanner.nextLine()
    var result:String=""
    while ( dni != "END"){
        if (employeeMap[dni]!=null) // = employeeMap.containsKey(dni)//
            {
            result += "${employeeMap[dni]}\n"
        } else println("employee doesn't exist")
        //result += employeeMap[dni] ?: "employee doesn't exist"
        dni = scanner.nextLine()
    }
    println(result)
}

fun readEmployee(scanner: Scanner): MutableMap<String, Empleat> {
    val count = scanner.nextInt()
    scanner.nextLine()
    var employeeMap = mutableMapOf<String, Empleat>()
    repeat(count){
        var dni = scanner.nextLine()
        employeeMap.put(dni, Empleat(dni, scanner.nextLine(), scanner.nextLine(),scanner.nextLine()))
    }
    return employeeMap
}

data class Empleat (var dni:String, var nom: String, var cognoms:String, var adress:String)
