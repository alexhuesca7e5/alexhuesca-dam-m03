package cat.itb.alexhuesca7e5.dam.m03.uf5.exercices.tads

import cat.itb.alexhuesca7e5.dam.m03.uf5.exercices.lists.Country
import cat.itb.alexhuesca7e5.dam.m03.uf5.exercices.lists.readCountry
import java.util.*


fun main() {
    val scanner = Scanner(System.`in`)
    val cartell = readCartell(scanner)
    val metres = readMeters(scanner)
    metres.forEach {
        if (cartell.containsKey(it)) println(cartell[it]) else println("no hi ha cartell")
    }

}

val readCartell :(Scanner) -> Map<Int,String> = { scanner ->
    val size = scanner.nextInt()
    val list = mutableMapOf<Int,String>()
    repeat(size){ list[scanner.nextInt()] = scanner.next() }
    list
}

val readMeters :(Scanner) -> List<Int> = { scanner ->
    val size = scanner.nextInt()
    val list = mutableListOf<Int>()
    repeat(size){list.add(scanner.nextInt())}
    list
}