package cat.itb.alexhuesca7e5.dam.m03.uf3.exercices

import kotlin.io.path.Path
import kotlin.io.path.createDirectories

fun main() {
    val homePath : String = System.getProperty("user.home")
    val path1 = Path(homePath, "dummyfolders")
    for (i in 1..100){
        val path = Path(path1.toString(), i.toString())
        path.createDirectories()
    }
}