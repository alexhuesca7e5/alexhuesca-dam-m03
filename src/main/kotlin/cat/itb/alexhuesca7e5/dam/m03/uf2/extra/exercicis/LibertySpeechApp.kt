package cat.itb.alexhuesca7e5.dam.m03.uf2.extra.exercicis

import java.util.*

data class Country(val name:String, val numDetected:Int, val seriousness:Double){
    fun calculatePunctuation() = numDetected * seriousness
}

fun main() {
    val scn = Scanner(System.`in`).useLocale(Locale.UK)
    val countryList = mutableListOf<Country>()
    val size = scn.nextInt()
    repeat(size){
        countryList.add(Country(scn.next(),scn.nextInt(),scn.nextDouble()))
    }
    println("---------- Paisos ----------")
    countryList.forEach { country -> println("${country.name} - casos: ${country.numDetected} - gravetat: ${country.seriousness} - puntuació: ${country.calculatePunctuation()}")}
    println("---------- Resum ----------\n"+
            "Casos totals: ${countryList.sumOf { it.numDetected }}\n"+
            "País amb més casos: ${countryList.maxByOrNull { it.numDetected }?.name}")
}