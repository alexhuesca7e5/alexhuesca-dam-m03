package cat.itb.alexhuesca7e5.dam.m03.uf5.exercices.tads

import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    val readCar = readCar(scanner)
    checkPlateNumber(scanner,readCar)

}

val readCar :(Scanner) -> MutableMap<String,Car> = { scanner ->
    val size = scanner.nextInt()
    val carsList = mutableMapOf<String,Car>()
    repeat(size){
        var plate = scanner.next()
        carsList.put(plate, Car(plate,scanner.next(), scanner.next()))}
    scanner.nextLine()
    carsList
}

val checkPlateNumber :(Scanner, MutableMap<String,Car>) -> String = { scanner, carsMap ->
    var carToSearch = scanner.nextLine()
    var cars:String=""
    while (carToSearch !="END"){
        cars += carsMap.get(carToSearch).toString()+"\n"
        carToSearch = scanner.nextLine()
    }
    println(cars)
    carToSearch
}


data class Car(var plate:String, var model: String, var color:String){}
