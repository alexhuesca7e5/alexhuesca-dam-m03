package cat.itb.alexhuesca7e5.dam.m03.uf1.iterative

import java.util.*

fun main() {
    val sc=Scanner(System.`in`)
    var num:Int=sc.nextInt()
    var sumaNum=sc.next()
    var residuNum= num % 10
    while(num!=0){
        residuNum=num%10
        num /= 10
        sumaNum += residuNum
    }
}