package cat.itb.alexhuesca7e5.dam.m03.uf1.lists

import java.util.*

fun main() {
    val inputValue = Scanner(System.`in`)
    val matrix = listOf(
       //x      0    1    2    3    4    5    6
        listOf("x", "x", "0", "0", "0", "0", "x"),//0
        listOf("0", "0", "x", "0", "0", "0", "x"),//1
        listOf("0", "0", "0", "0", "0", "0", "x"),//2
        listOf("0", "x", "x", "x", "0", "0", "x"),//3
        listOf("0", "0", "0", "0", "x", "0", "0"),//4
        listOf("0", "0", "0", "0", "x", "0", "0"),//5
        listOf("x", "0", "0", "0", "0", "0", "0")//6
                                                  //y
    )
    val x= inputValue.nextInt()
    val y=inputValue.nextInt()
    val result= matrix[x][y]
    if(result== "x"){
        println("tocat")}
    if(result== "0"){
        println("aigua")
    }
}