package cat.itb.alexhuesca7e5.dam.m03.uf1.seleccio

import java.util.*

fun main() {
    val scn = Scanner(System.`in`)
    println("Cabells:")
    val cabells: String = scn.next()
    println("ulls:")
    val ulls: String = scn.next()
    println("nas:")
    val nas: String = scn.next()
    println("boca:")
    val boca: String = scn.next()
    when (cabells) {
        "arrissats" -> println("@@@@@")
        "llisos" -> println("VVVVV")
        "pentinats" -> println("XXXXX")
        else -> println("Error")
    }
    when (ulls) {
        "aclucats" -> println(".-.-.")
        "rodons" -> println(".o-o.")
        "estrellats" -> println(".*-*.")
        else -> println("Error")
    }
    when (nas){
        "aixafat"-> println("..0..")
        "arromangat"-> println("..C..")
        "agilenc"-> println("..V..")
        else-> println("Error")
    }
    when(boca){
        "normal"-> println(".===.")
        "bigoti"-> println(".∼∼∼.")
        "dents-sortides"-> println(".www.")
        else-> println("Error")
    }
}