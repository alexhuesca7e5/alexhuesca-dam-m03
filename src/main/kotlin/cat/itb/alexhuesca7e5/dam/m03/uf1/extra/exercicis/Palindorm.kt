package cat.itb.alexhuesca7e5.dam.m03.uf1.extra.exercicis

import java.util.*

fun main() {
    val scn = Scanner(System.`in`)
    val sentence2=scn.nextLine().replace("[ .,'!?-]".toRegex(),"").lowercase()
    val reversedSentence = sentence2.reversed()
    println(reversedSentence == sentence2)
    //val sentence = scn.nextLine().replace(".","").replace(",","")
    //.replace("'","").replace("!","").replace("?","").replace("-","").replace(" ","")
    //.lowercase(Locale.getDefault())
    //println(sentence == sentence.reversed())
}