package cat.itb.alexhuesca7e5.dam.m03.extraordinaria.uf1

import java.util.*

fun main() {
    val scn= Scanner(System.`in`).useLocale(Locale.UK)
    var fish:String = ""
    var price:Double = 0.0
    var lastPrice = 0.0
    while (true){
        fish = scn.next()
        if (fish=="END")break
        price = scn.nextDouble()
        if(fish == "rap"){
            if ((lastPrice-5) > price){
                println("Comprat a $price")
            }
            lastPrice = price
        }
    }
}