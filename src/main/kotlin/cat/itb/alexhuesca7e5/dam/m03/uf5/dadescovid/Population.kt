package cat.itb.alexhuesca7e5.dam.m03.uf5.dadescovid

import kotlinx.serialization.Serializable

@Serializable
class Population : ArrayList<PopulationItem>()

{
    fun printSpainDeaths(){
        val deaths = Population().find { it.code == "ESP" }?.population
        println("Deaths: $deaths")


       /* println(
            deaths.joinToString (separator = "\n", limit = 10, prefix = "#### Most deaths: #### \n"){ country ->  "${countries.indexOf(country)+1}.${country.Country}: ${country.TotalDeaths}" }
        )
        */
    }
}