package cat.itb.alexhuesca7e5.dam.m03.uf2.recursivity

import java.util.*

fun dotLineRecursive(num :Int):String{
    return if (num==0) ""
    else
        "." + dotLineRecursive(num-1)
}

fun main() {
    val scn= Scanner(System.`in`)
    val inputValue= scn.nextInt()
    println(dotLineRecursive(inputValue))
}
