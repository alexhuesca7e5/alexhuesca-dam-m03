package cat.itb.alexhuesca7e5.dam.m03.uf6.beachapp.data

import cat.itb.alexhuesca7e5.dam.m03.uf6.beachapp.models.Beach
import java.sql.Connection
import java.sql.ResultSet

class BeachesDao(val database: BeachesDatabase) {
    val connection: Connection get() = database.connection!!

    fun createTableIfNotExists() {
        val createQuery = "CREATE TABLE IF NOT EXISTS beaches(id INTEGER PRIMARY KEY, name VARCHAR, city VARCHAR, quality INTEGER)"
        val createStatement = connection.prepareStatement(createQuery)
        createStatement.execute()
    }

    fun list(): List<Beach> {
        val query = "SELECT * FROM beaches"
        val listStatement = connection.createStatement()
        val resultat: ResultSet = listStatement.executeQuery(query)
        return toBeachList(resultat)
    }

    fun insert(beach: Beach) {
        val query = "INSERT INTO beaches(id,name,city,quality) VALUES(?, ?, ?, ?)"
        val statment = connection.prepareStatement(query)
        statment.setInt(1, beach.id)
        statment.setString(2, beach.name)
        statment.setString(3, beach.city)
        statment.setInt(4, beach.quality)
        statment.execute()
    }
    /**
     * Converts result set to list of beaches
     */
    private fun toBeachList(result: ResultSet): List<Beach> {
        val list: MutableList<Beach> = mutableListOf()
        while (result.next()) {
            list += toBeach(result)
        }
        return list
    }

    /**
     * Converts result set to a beach on the current position of the set
     */
    private fun toBeach(result: ResultSet): Beach {
        val id = result.getInt("id")
        val name = result.getString("name")
        val city = result.getString("city")
        val quality = result.getInt("quality")
        return Beach(id, name, city,quality)
    }

    fun updateQuality(beach: Beach){
        val updateQuery = "UPDATE beaches SET(quality) = ? WHERE id = ?"
        val updateStatement = connection.prepareStatement(updateQuery)
        updateStatement.setInt(1, beach.quality)
        updateStatement.setInt(2, beach.id)
        updateStatement.execute()
    }
}