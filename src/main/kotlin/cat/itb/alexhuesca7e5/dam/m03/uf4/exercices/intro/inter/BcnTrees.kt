package cat.itb.alexhuesca7e5.dam.m03.uf4.exercices.intro.inter

import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.request.*
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import java.io.File
import java.util.*

@Serializable
data class ArbresItem(
    val adreca: String?,
    val cat_especie_id: Int?,
    val catalogacio: String?,
    val categoria_arbrat: String?,
    val codi: String?,
    val codi_barri: String?,
    val codi_districte: String?,
    val data_plantacio: String?,
    val espai_verd: String?,
    val geom: String?,
    val latitud: String?,
    val longitud: String?,
    val nom_barri: String?,
    val nom_castella: String?,
    val nom_catala: String?,
    val nom_cientific: String?,
    val nom_districte: String?,
    val tipus_aigua: String?,
    val tipus_element: String?,
    val tipus_reg: String?,
    val x_etrs89: String?,
    val y_etrs89: String?
)
interface BcnTreeDataSource {
    suspend fun listTrees(): List<ArbresItem>
}
class LocalBcnTreeDataSource():BcnTreeDataSource{
    override suspend fun listTrees(): List<ArbresItem> {
        val json = File("src/main/resources/cat/itb/alexhuesca7e5/dam/m03/uf3/exercices","arbres.json")
        val trees = Json.decodeFromString<List<ArbresItem>>(json.readText())
        return trees
    }
}
class APIBcnTreeDataSource():BcnTreeDataSource{
    override suspend fun listTrees(): List<ArbresItem> {
        val client = HttpClient(CIO){
            install(JsonFeature){
                serializer = KotlinxSerializer(kotlinx.serialization.json.Json {
                    ignoreUnknownKeys = true
                })
            }
        }
        val trees : List<ArbresItem> = client.get("https://opendata-ajuntament.barcelona.cat/resources/bcn/Arbrat/OD_Arbrat_Zona_BCN.json")
        return trees
    }

}

suspend fun main(){
    val scn = Scanner(System.`in`)
    val option =  scn.nextLine()
    val bcnTreeDataSource : BcnTreeDataSource = if(option == "NETWORK") APIBcnTreeDataSource() else LocalBcnTreeDataSource()
    val treeName = scn.nextLine()
    val trees = bcnTreeDataSource.listTrees()
    println(trees.count{tree -> tree.nom_cientific == treeName})
}

