package cat.itb.alexhuesca7e5.dam.m03.uf1.seleccio

import java.time.Year
import java.util.*

fun main(){
    val sc= Scanner(System.`in`)
    val month= sc.nextInt()
    if(month>12)
        println("Error")
    when (month){
        1,3,5,7,8,10,12 -> println(31)
        4,6,9,11 -> println(30)
        2 -> println("28 o 29")
    }
}
