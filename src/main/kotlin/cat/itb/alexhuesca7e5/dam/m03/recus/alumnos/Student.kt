package cat.itb.alexhuesca7e5.dam.m03.recus.alumnos

data class Student(val name:String, val mark:Double)