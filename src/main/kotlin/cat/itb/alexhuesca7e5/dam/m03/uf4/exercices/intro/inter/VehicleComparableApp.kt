package cat.itb.alexhuesca7e5.dam.m03.uf4.exercices.intro.inter


interface Comparable<T> {
    fun compareTo(other: T): Int
}

abstract class Model : Comparable<Model> {
    abstract val name:String
    abstract val brand: VehicleBrand
    abstract override fun toString():String
    override fun compareTo(other: Model): Int {
        return name.compareTo(other.name)
    }
}
data class VehicleBrand(val name: String, val country: String) {
    override fun toString(): String {
        return "name:$name, country:$country"
    }
}

data class BicycleMod(override val name: String, val gears: Int, override val brand: VehicleBrand) : Model() {
    override fun toString(): String {
        return "name:$name, gears:$gears, brand:$brand"
    }
}

class ScootereModel(override val name: String, val power: Double, override val brand: VehicleBrand) : Model() {
    override fun toString(): String {
        return "name:$name, power:$power,brand:$brand"
    }
}

fun main() {
    val bici = BicycleMod("B",5, VehicleBrand("Lorem","Togo"))
    val bici2 = BicycleMod("kotlin",5, VehicleBrand("Lorem","Togo"))
    val scooter = ScootereModel("Hotwalk",45.3, VehicleBrand("lorem","Togo"))
    val scooter2 = ScootereModel("Alex",45.3, VehicleBrand("lorem","Togo"))
    val list :List<Model> = mutableListOf<Model>(bici,scooter,bici2,scooter2)
    println(list.sortedBy { it.name })
}