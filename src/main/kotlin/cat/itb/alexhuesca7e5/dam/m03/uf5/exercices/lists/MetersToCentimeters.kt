package cat.itb.alexhuesca7e5.dam.m03.uf5.exercices.lists

import java.util.*

//fun readList(pato: Scanner): List<Int> {
//    return scanner.nextInt()
//}

fun main() {
    val scanner = Scanner(System.`in`)
    val integerList = readIntList(scanner)
    integerList.forEach { num -> println(num*1000) }

}

val readIntList :(Scanner) -> List<Int> = { scanner ->
    val size = scanner.nextInt()
    val list = mutableListOf<Int>()
    repeat(size){list.add(scanner.nextInt())}
    list
}