package cat.itb.alexhuesca7e5.dam.m03.uf1.mix

import java.util.*

    fun main() {
        val scn= Scanner(System.`in`)
        val word=scn.nextLine().replace(" ","").replace(".","").replace(",","")
            .replace("'","").replace("!","").replace("?","")
            .replace("-","").lowercase(Locale.getDefault())
        //val word=scn.nextLine().replace("[^a-zA-Z0-9]".toRegex(),"")
        //println(word)
        println(word==word.reversed())
    }
