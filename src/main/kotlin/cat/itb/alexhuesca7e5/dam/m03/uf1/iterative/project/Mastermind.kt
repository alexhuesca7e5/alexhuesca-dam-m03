package cat.itb.alexhuesca7e5.dam.m03.uf1.iterative.project
import java.util.*

data class Evaluation(val rightPosition: Int, val wrongPosition: Int)
fun main() {
    val scanner = Scanner(System.`in`)
    println("Vols jugar en mode 1vs1 (1) o solitari (2)?")
    val gameMode = scanner.nextInt()
    var secret=""
    if (gameMode == 1) {
        println("Introdueix la paraula secreta")
        secret = scanner.next()
    }else{
        repeat(4) { secret += randomChar() }
    }
    println("Comença el joc!")
    var round = 1
    while(round<=12) {
        println("Introdueix una combinació")
        val combination= evaluateWord(secret,scanner.next())
            if (combination.rightPosition==4){
            println("Enhorabona! has guanyat")
                break
        }
        println("Posicions correctes:${combination.rightPosition}, Posicions Incorrectes:${combination.wrongPosition}")
        if(round==12) println("Fi del joc. Has perdut!")
        round++

    }

}
fun randomChar():Char{
    val allowedChars = 'A'..'F'
    return allowedChars.random()
}

fun evaluateWord(secret: String, guess: String): Evaluation {
    var rightPosition = 0
    var wrongPosition = 0
    for(i in secret.indices) {
        if (secret[i] == guess[i]){
            rightPosition ++
        } else if (guess[i] in secret) {
            wrongPosition++
        }
    }
return Evaluation(rightPosition,wrongPosition)
}

