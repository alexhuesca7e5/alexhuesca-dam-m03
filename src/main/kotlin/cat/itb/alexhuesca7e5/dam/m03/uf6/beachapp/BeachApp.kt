package cat.itb.alexhuesca7e5.dam.m03.uf6.beachapp

import cat.itb.alexhuesca7e5.dam.m03.uf6.beachapp.data.BeachesDatabase
import cat.itb.alexhuesca7e5.dam.m03.uf6.beachapp.ui.BeachApp
import java.util.*


fun main() {
    val scanner = Scanner(System.`in`)
    val database = BeachesDatabase()
    BeachApp(scanner, database).start()
}