package cat.itb.alexhuesca7e5.dam.m03.uf1.iterative

import java.util.*

fun main() {
    val scanner= Scanner(System.`in`)
    var number= scanner.nextInt()
    var anterior= 0
    var result= 1
    var aux= 0
    while(result <= number){
        aux= result
        result= result + anterior
        anterior= aux
    }
    println(anterior)
}