package cat.itb.alexhuesca7e5.dam.m03.uf1.lists.exam

import java.util.*

fun main() {
    val list= readIntList(Scanner(System.`in`))
    var changes=0
    for(i in 2..list.lastIndex){
        if(list[i-2]>list[i-1] && list[i-1]<list[i] ||
            list[i-2]<list[i-1] && list[i-1] > list[i]){
            changes++
        }
    }
    println("Té $changes canvis pendents")
}