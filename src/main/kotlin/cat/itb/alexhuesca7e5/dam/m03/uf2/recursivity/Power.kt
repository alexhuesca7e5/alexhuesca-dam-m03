package cat.itb.alexhuesca7e5.dam.m03.uf2.recursivity

import java.util.*

fun power(a:Int,b:Int): Int{
    if ( b == 0)
        return 1
    else
        return a * power(a,b-1)
}

fun main() {
    val scn= Scanner(System.`in`)
    val base = scn.nextInt()
    val exponent = scn.nextInt()
    println(power(base,exponent))
}