package cat.itb.alexhuesca7e5.dam.m03.uf1.lists

import java.util.*

fun main() {
    val inputValue= Scanner(System.`in`)
    val table= List(8){ MutableList (8){"x"}}
    val letters= listOf("a","b","c","d","e","f","g","h")
    val numbers= listOf(8,7,6,5,4,3,2,1)
    val inputLetter= letters.indexOf(inputValue.next()) //el indexOf te saca el index de un valor de la lista
    val inputNumber= numbers.indexOf(inputValue.nextInt())
    table[inputNumber].replaceAll {"♖"}//remplaza todo los valores de la fila que mete en usuario
    for (fila in table) {
        fila[inputLetter]="♖" //recorre las filas y cambia el valor de la posición que ha introducido el usuario
    }
    table[inputNumber][inputLetter]="♜"
   table.forEach{ println(it)}
}