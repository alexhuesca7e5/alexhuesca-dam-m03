package cat.itb.alexhuesca7e5.dam.m03.uf4.exercices.intro


open class MechanicalArm (var openAnlge:Double = 0.0, var altitude:Double = 0.0, var turnedOn:Boolean = false){
    fun toggle(){
        this.turnedOn = !turnedOn
        if (!this.turnedOn) {
            openAnlge = 0.0
            altitude = 0.0
        }
        println(this)
    }

    fun updateAltitude(num:Double){
        this.altitude =+ num
        if (altitude < 0.0){
            altitude = 0.0
        }
        println(this)
    }
    fun updateAngle(ang:Double){
        this.openAnlge =+ ang
        if (openAnlge >= 360){
            openAnlge = 360.0
        }
        if (openAnlge < 0.0){
            openAnlge = 0.0
        }
        println(this)
    }
    override fun toString(): String {
        return "openAngle:$openAnlge, altitude:$altitude, turnedOn:$turnedOn"
    }
}
