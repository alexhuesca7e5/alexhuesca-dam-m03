package cat.itb.alexhuesca7e5.dam.m05.uf1

import kotlin.math.abs

fun main(args: Array<String>) {
    val numero1 = 548745184
    val numero2 = 25145
    var result: Long = 0
    for (i in 0 until numero2) {
        result += numero1
    }
    println("la multiplicació de $numero1 i $numero2 es $result")

}
