package cat.itb.alexhuesca7e5.dam.m05.uf1

fun main() {
    howManyDays(2,2020)
    println(howManyDays(2,2020))
}
fun howManyDays(month: Int, year: Int): Int {
    return when (month) {
        4, 6, 9, 11 -> 30
        2 -> {
            if (isLeapYear(year)) 29 else 28
        }
        else -> 31
    }
}

fun isLeapYear(year: Int): Boolean {
    var isLeapYear: Boolean
    if (year % 4 == 0) {
        if (year % 100 != 0) {
            isLeapYear= true
        } else if (year % 400 == 0) {
            isLeapYear= false
        } else {
            isLeapYear= false
        }
    } else {
        isLeapYear = false
    }
    return isLeapYear
}



