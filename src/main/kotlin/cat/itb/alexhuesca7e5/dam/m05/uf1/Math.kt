package cat.itb.alexhuesca7e5.dam.m05.uf1

import kotlin.math.sqrt

fun main() {
    var result = 0.0
    for (i in 0..9999) {
        if (result > 100) {
            result = sqrt(result)
        }
        if (result < 0) {
            result += result * result
        }
        result += 20.2
    }
    print("el resultat és $result")
}