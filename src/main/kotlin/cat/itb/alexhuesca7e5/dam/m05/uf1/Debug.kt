package cat.itb.alexhuesca7e5.dam.m05.uf1

fun main() {
    val start = 7
    val end = 1252
    var tempValue1 = 1548
    var tempValue2 = -457
    var tempValue3 = 254
    for (i in start..end) {
        tempValue1 = tempValue1 - tempValue2
        tempValue2 += Math.log(tempValue1.toDouble()).toInt()
        tempValue3 -= i
    }
    println("tempValue1 = $tempValue1")
    println("tempValue2 = $tempValue2")
    println("tempValue3 = $tempValue3")
}