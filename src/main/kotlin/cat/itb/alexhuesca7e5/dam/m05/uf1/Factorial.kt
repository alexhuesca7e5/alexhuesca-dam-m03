package cat.itb.alexhuesca7e5.dam.m05.uf1

fun main() {
    factorial(5)

}

fun factorial(number: Int): Int {
    var result = 1
    for (i in 1..number) {
        result *= i
    }
    return number
}

